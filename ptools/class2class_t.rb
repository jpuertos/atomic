# -*- coding: utf-8 -*-
#
# Rename 'class' to 'class_' in ALL TOMENET code.
# 
# Important not to interfere with rest of the code, like comments and so.
#
# This is an Ah-hoc method. we are not parsing any C code here... Faster than using any BISON or YACC believe me.
# and less reliable, for sure!
#

require 'fileutils'
require 'tempfile'

def process_code(fname)
  t_file = Tempfile.new('processed_temp.txt')
  f = File.open(fname, "r")
  code = ""     #Part of line that is code.
  comment = ""  #Part of line that is comment.
  block = false
  f.each_line do |l|
    if (l =~ /\/\*[^{\*\/}]*$/) # Begin of a comment block
      block = true
      code = $`
      comment = $&
      code.gsub!(/(\W)(class)(\W)/, '\1class_\3') # WTF? only works once...
      t_file << code << comment << $'
      next
    end

    if (l =~ /^[^{\*\/}]*\*\//) # End of a comment blook   
      block = false
      t_file << l
      next
    end
    
    if (block == true) # Skipe lines within a comment block.
      t_file << l
      next
    end

    # Comment style: code /* something */
    if (l =~ /\/\*.*\*\//)
      code = $`
      comment = $&
      code.gsub!(/(\W)(class)(\W)/, '\1class_\3') # WTF? only works once...
      t_file << code << comment << $'
      next
    end

    # Comment style: // soemthing
    if (l =~ /\/\/.*/)
      code = $`
      comment = $&
      code.gsub!(/(\W)(class)(\W)/, '\1class_\3') # WTF? only works once...
      t_file << code << comment << $'
      next
    end

    code = l
    code.gsub!(/(\W)(class)(\W)/, '\1class_\3') # WTF? only works once...
    t_file << code
  end

  f.close
  t_file.close
  FileUtils.mv(fname, "#{fname}.bak")
  FileUtils.mv(t_file.path, fname)
end

def filetree_process(dir)
  dir.each do |e|
    if (e != ".." and e != "." and File.directory?(dir.path+e))
      puts " --> Entering directory: #{e}"
      dir_aux = Dir.new(dir.path+e)
      filetree_process(dir_aux)
    end
    if e =~ /\w+\.[c|h]$/ # If it's a source code file.
      puts "    *Processing #{e}"
      process_code(dir.path+"/"+e)
    end
    
  end
end

dir = Dir.new(ARGV[0])
filetree_process(dir)
