def scan_file(fs, prefix_re, scan_re, rewind)
  labels = []
  if rewind then fs.rewind end
  fs.each_line do |l|
    if l =~ prefix_re
      labels << l.scan(scan_re)
    end
  end
  labels.flatten!
  labels.each { |v| v.strip! }
  return labels.uniq!.sort!
end

def scan_line_fields(line, fields_re)
  md = []
  md = fields_re.match(line)
  return md[1, md.size]
end

#
# C++ reserved words that may appear as flags, or whatever.
#
def make_valid_id(label)
  reserved = ["friend", "class", "int", "double", "protected", "delete"] 
  reserved.each do |r|
    if label.downcase == r
      label << "_"
      return label.downcase
    end
  end

  # For the R_CHAR_e R_CHAR_E we need to be case sensitive.
  if label =~ /R_CHAR_(.)/
    return "r_char_#{$1}"
  end
    
  return label.downcase.gsub(/^1/, "one")
end

def gencode_enum(labels, datatype, has_none, has_counter)
  code = "enum class #{datatype} {\n"
  if has_none
    code << "    _none_,\n"
  end
  labels.each { |l| code << "    #{make_valid_id(l)},\n" }
  if has_counter
    code << "    _size_\n"
  end
  code << "};\n"
  return code
end


def gencode_flags_bitset_string(enumtype, flags_present, flags_set)
  bin_string = ""
  flags_present.each do |f|
    if flags_set.index(f) == nil
      bin_string << "0"
    else
      bin_string << "1"
    end
  end
  return "std::bitset<std::size_t(#{enumtype}::_size_)>(std::string(\"#{bin_string}\"))"
end

def gencode_vector_flags(enumtype, flags)
  code = "std::vector<#{enumtype}>{"
  flags.each do |f|
    code << "#{enumtype}::#{make_valid_id(f)}, "
  end
  code << "}"
  return code
end

def gen_tile(char, attr, typename)
  if char =~ /(\\|'|")/
    char = "\\" + char
  end    
  return "#{typename}(\'#{char}\', \'#{attr}\')"
end

def insert_code(code, file_name, entry_point_regexp)
  fs_input = File.new(file_name, "r")
  fs.each_line do |l|
    if l =~ entry_point_regexp
      fs << code
      break
    end
    fs_input.close
  end
end
