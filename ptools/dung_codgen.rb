require_relative 'codegen_common.rb'

def gencode_dungeon_entries(fs_in, container_name, flags_list)
  code = ""
  fs_in.rewind
  fs_in.each_line do |l|
    tome_ind = 0
    name = ""
    short_name = ""
    long_name = ""
    xinfo_init = "{"
    floor_init = "FeatureList{"
    floor_end_init = ""
    wall_init = "FeatureList{"
    wall_end_init = ""
    loot_init = "{"
    flags = []

    if l =~ /^N\:(\d+)\:(.+)/
      tome_ind = $1 
      name = $2.strip
      until ( (lsub = fs_in.gets) == "\n" )
        if lsub =~ /^D\:(.*)\:(.*)/
          short_name = $1
          long_name = $2
          next
        end

        if lsub =~ /^W\:(\d+):(\d+):(\d+):(\d+):(\d+):(\d+)/
          xinfo_init << "#{$1}, #{$2}, #{$3}, #{$4}, #{$5}, #{$6}"
          next
        end

        if lsub =~ /^L\:(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+)$/
          floor_init << "FeatureProb(#{$1}, #{$2}), "
          floor_init << "FeatureProb(#{$3}, #{$4}), "
          floor_init << "FeatureProb(#{$5}, #{$6}), "
          floor_init << "FeatureProb(#{$7}, #{$8}), "
          floor_init << "FeatureProb(#{$9}, #{$10})}"
          next
        end

        if lsub =~ /^L\:(\d+):(\d+):(\d+):(\d+):(\d+)$/
          floor_end_init << "std::vector<std::size_t>{#{$1}, #{$2}, #{$3}, #{$4}, #{$5}}"
          next
        end

        if lsub =~ /^A\:(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+):(\d+)/
          wall_init << "FeatureProb(#{$1}, #{$2}), "
          wall_init << "FeatureProb(#{$3}, #{$4}), "
          wall_init << "FeatureProb(#{$5}, #{$6}), "
          wall_init << "FeatureProb(#{$7}, #{$8}), "
          wall_init << "FeatureProb(#{$9}, #{$10}), "
          wall_init << "FeatureProb(#{$11}, #{$12})}"
          next
        end

        if lsub =~ /^A\:(\d+):(\d+):(\d+):(\d+):(\d+)$/
          wall_end_init << "std::vector<std::size_t>{#{$1}, #{$2}, #{$3}, #{$4}, #{$5}}"
          next
        end

        if lsub =~ /^O\:(\d+):(\d+):(\d+):(\d+)/
          loot_init << "#{$1}, #{$2}, #{$3}, #{$4}"
          next
        end
        if lsub =~ /^F\:/
          flags << lsub.scan(/(\w+)[\s|\n]/)
          flags.flatten!
          next
        end
      end

      if (floor_end_init == "")
        floor_end_init = "{}"
      end

      if (wall_end_init == "")
        wall_end_init = "{}"
      end

      xinfo_init << "}"
      loot_init << "}"
      flags_init = gencode_flags_bitset_string("DungeonFlag", flags_list, flags)
      d_info_init = "{#{tome_ind}, \"#{name}\", \"#{short_name}\", \"#{long_name}\", "
      d_info_init << "#{xinfo_init}, #{floor_init}, #{floor_end_init}, #{wall_init}, #{wall_end_init}, #{loot_init}, #{flags_init}}"
      code << "  #{container_name}.insert(#{d_info_init});\n"
    end
  end
return code
end

#Path to Tomenet.
game_path = ARGV[0]

fs_dung = File.new(game_path + "/lib/game/d_info.txt", "r")

flags = scan_file(fs_dung, /^F\:/, /(\w+)[\s|\n]/, true)

fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/DungeonInfo.skel.h", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/DungeonInfo.h", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Enums/
    fs_filled << gencode_enum(flags, "DungeonFlag", false, true) << "\n"
  end
end
fs_skel.close
fs_filled.close

fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/DungeonCatalog.skel.cpp", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/DungeonCatalog.cpp", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Insert dung/
    fs_filled << gencode_dungeon_entries(fs_dung, "dungeon_type", flags)
  end
end
fs_skel.close
fs_filled.close
fs_dung.close

