require_relative 'codegen_common.rb'

def gencode_kind_entries(fs_in, container_name, flags_list)
  code = ""
  fs_in.rewind
  fs_in.each_line do |l|
    a = ""
    c = ""
    tome_ind = 0
    name = ""
    info_init = "{"
    xinfo_init = "{"
    power_init = "{"
    flags = []
    #
    # PARTS MISSING HERE.
    #

    if l =~ /^N\:(\d+)\:(.+)/
      tome_ind = $1 
      name = $2.strip
      until ( (lsub = fs_in.gets) == "\n" )
        if lsub =~ /G\:(.)\:(\w)/
          c = $1
          a = $2
          next
        end
        if lsub =~ /^I\:(\d+):(\d+):(\d+)/
          info_init << "#{$1}, #{$2}, #{$3}"
          next
        end
        if lsub =~ /^W\:(\d+):(\d+):(\d+):(\d+)/
          xinfo_init << "#{$1}, #{$2}, #{$3}, #{$4}"
          next
        end
        if lsub =~ /^P\:(\d+):(\d+)d(\d+):(\d+):(\d+):(\d+)/
          power_init << "#{$1}, #{$2}, #{$3}, #{$4}, #{$5}, #{$6}"
          next
        end
        if lsub =~ /^F\:/
          flags << lsub.scan(/(\w+)[\s|\n]/)
          flags.flatten!
          next
        end
      end

      info_init << "}"
      xinfo_init << "}"
      power_init << "}"

      flags_init = gencode_flags_bitset_string("KindFlag", flags_list, flags)
      k_info_init = "{#{tome_ind}, #{gen_tile(c, a, "Tile")}, \"#{name}\", #{info_init}, #{xinfo_init}, #{power_init}, #{flags_init}}" 
      code << "  #{container_name}.insert(#{k_info_init});\n"
    end
  end
  return code
end

#Path to Tomenet.
game_path = ARGV[0]

fs_kind = File.new(game_path + "/lib/game/k_info.txt", "r")
flags = scan_file(fs_kind, /^F\:/, /(\w+)[\s|\n]/, true)

#
# KindInfo.h
#
fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/KindInfo.skel.h", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/KindInfo.h", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Enums/
    fs_filled << gencode_enum(flags, "KindFlag", false, true) << "\n"
  end
end
fs_skel.close
fs_filled.close

#
# KindCatalog.cpp, Insert entries.
#
fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/CatalogKind.skel.cpp", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/CatalogKind.cpp", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Insert/
    fs_filled << gencode_kind_entries(fs_kind, "tile_index", flags)
  end
end

fs_skel.close
fs_filled.close
fs_kind.close
