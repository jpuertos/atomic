# -*- coding: utf-8 -*-

require_relative 'codegen_common.rb'

# S: spell frequency |
# S: spell type | spell type | etc
# D: Description


###### Understanding the entries ######

# N: serial number : monster name
# G: symbol : color
# I: speed : hit points : vision : armour class : alertness
# W: depth : rarity : corpse weight : experience for kill
# E: weapons : torso : arms : finger : head : leg
# O: treasure : combat : magic : tool
# B: attack method : attack effect : damage
# F: flag | flag | etc
# S: spell frequency |
# S: spell type | spell type | etc
# D: Description

def get_attack_method_and_effect_list(fs)
  methods = []
  effects = []
  fs.rewind
  fs.each_line do |l|
    if l =~ /^B\:(\w+)\:(\w+)/
      methods << $1.downcase
      effects << $2.downcase
    end
  end
  return methods.uniq!.sort!, effects.uniq!.sort!
end

def fix_flags_with_char(c, flags)
  char_flag = false
  if c =~ /[A-Za-z]/
    flags.each do |f|
      if f =~ /R_CHAR_#{c}/
        char_flag = true
        break
      end
    end
  end
  if char_flag
    flags << "R_CHAR_#{c}"
  end
end

#
# TODO: Proces "$" Entries, like RPG_SERVER, or HALLOWEEN.
#
def gencode_race_entries(fs_in, container_name, flags_list)
  code = ""
  fs_in.rewind
  fs_in.each_line do |l|
    a = ""
    c = ""
    tome_ind = 0
    name = ""
    desc = ""
    desc_already = false
    info_init = "{"
    xinfo_init = "{}" #Hack to solve the #872 Balrog of Moria duplicated W: entry.
    slots_init = "{"
    drop_init = "{"
    blow_init = "{"
    spell_init = "{"
    spell_types = []
    flags = []

    if l =~ /^N\:(\d+)\:(.+)/
      tome_ind = $1 
      name = $2.strip
      has_spells = false
      until ( (lsub = fs_in.gets) == "\n" )
        if lsub =~ /G\:(.)\:(\w)/
          c = $1
          a = $2
          next
        end
        if lsub =~ /^I\:(\d+):(\d+)d(\d+):(\d+):(\d+):(\d+)/
          info_init << "#{$1}, #{$2}, #{$3}, #{$4}, #{$5}, #{$6}"
          next
        end
        if lsub =~ /^W\:(\d+):(\d+):(\d+):(\d+)/
          xinfo_init = "{#{$1}, #{$2}, #{$3}, #{$4}}" #Hack to solve the #872 Balrog of Moria duplicated W: entry.
          next
        end
        if lsub =~ /^E\:(\d+):(\d+):(\d+):(\d+):(\d+):(\d+)/
          slots_init << "#{$1}, #{$2}, #{$3}, #{$4}, #{$5}, #{$6}"
          next
        end      
        if lsub =~ /^O\:(\d+):(\d+):(\d+):(\d+)/
          drop_init << "#{$1}, #{$2}, #{$3}, #{$4}"
          next
        end      
        if lsub =~ /^B\:(\w+):(\w+):(\d+)d(\d+)/
          if blow_init == "{"
            blow_init = "std::vector<RaceBlow>{"
          end
          blow_init << "{AttackMethod::#{$1.downcase}, AttackEffect::#{$2.downcase}, #{$3}, #{$4}},"
          next
        end
        if lsub =~ /^S\:(1_IN_\d+)[\s|\n]/
          has_spells = true
          rest = $'
          spell_init << "SpellFreq::#{make_valid_id($1)}, "
          spell_types << rest.scan(/(\w+)[\s|\n]/)
          next
        end
        if lsub =~  /^S\:\D/
          spell_types << lsub.scan(/(\w+)[\s|\n]/)
          next
        end
        if lsub =~ /^F\:/
          flags << lsub.scan(/(\w+)[\s|\n]/)
          flags.flatten!
          next
        end
        if lsub =~ /^D\:(.+)/
          if desc_already
            desc << " "
          end
          desc << $1.strip.gsub(/\"/, "\\\"")
          desc_aready = true
          next
        end
      end

      # Close open blocks.
      info_init << "}"
      blow_init << "}"
      #xinfo_init << "}"
      slots_init << "}"
      drop_init << "}"
      
      # Add spells and close block.
      if has_spells
        spell_types.flatten!
        spell_init << gencode_vector_flags("SpellType", spell_types) << "}"
      else
        spell_init << "SpellFreq::_none_, #{gencode_vector_flags("SpellType", spell_types)}}"
      end

      # Init for Race flags bitset.
      fix_flags_with_char(c, flags_list)
      flags_init = gencode_flags_bitset_string("RaceFlag", flags_list, flags)

      # Assemble all
      race_info_init = "{#{tome_ind}, #{gen_tile(c, a, "Tile")}, \"#{name}\", "
      race_info_init << "#{info_init}, #{xinfo_init}, #{slots_init}, "
      race_info_init << "#{drop_init}, #{blow_init}, #{spell_init}, #{flags_init}}"
      
      # Insert into container.
      code << "  #{container_name}.insert(#{race_info_init});\n"

     end
  end
  return code
end

#Path to Tomenet.
game_path = ARGV[0]

fs_race = File.new(game_path + "/lib/game/r_info.txt", "r")
fs_race_ego = File.new(game_path + "/lib/game/re_info.txt", "r")

# We parse the different flags and types.
methods, effects = get_attack_method_and_effect_list(fs_race)

#Spells
st = scan_file(fs_race, /^S\:\D/, /(\w+)[\s|\n]/, true)
st << scan_file(fs_race_ego, /^S\:\D/, /(\w+)[\s|\n]/, true)
st << "MF_ALL"
st << "SAME"
st.flatten!.uniq!.sort!

sf = scan_file(fs_race, /^S\:1_IN/, /^S\:(1_IN_\d+)[\s|\n]/ , true)
sf << scan_file(fs_race_ego, /^S\:1_IN/, /^S\:(1_IN_\d+)[\s|\n]/ , true)
sf.flatten!.uniq!.sort!

flags = scan_file(fs_race, /^F\:/, /(\w+)[\s|\n]/, true)
flags << scan_file(fs_race_ego, /^[F|H|M|O]\:/, /(\w+)[\s|\n]/, true)
flags.flatten!.uniq!.sort!

fs_race_ego.close

# For Ego mod stuff
methods << "MF_ALL"
methods << "SAME"
effects << "MF_ALL"
effects << "SAME"

#
# RaceInfo.h
#
fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/RaceInfo.skel.h", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/RaceInfo.h", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Enums/
    fs_filled << gencode_enum(methods, "AttackMethod", false, false) << "\n"
    fs_filled << gencode_enum(effects, "AttackEffect", false, false) << "\n"
    fs_filled << gencode_enum(sf, "SpellFreq", true, false) << "\n"
    fs_filled << gencode_enum(st, "SpellType", true, false) << "\n"
    fs_filled << gencode_enum(flags, "RaceFlag", false, true) << "\n"    
  end
end
fs_skel.close
fs_filled.close

#
# RaceCatalog.cpp, Insert entries.
#
fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/CatalogRace.skel.cpp", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/CatalogRace.cpp", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Insert/
    fs_filled << gencode_race_entries(fs_race, "tile_index", flags)
  end
end

fs_skel.close
fs_filled.close
fs_race.close
