# -*- coding: utf-8 -*-

require_relative 'codegen_common.rb'


# N:x:ego name
# G:x:y     x is the char, y the attribute, * means the normal one
# I:speed:(dice)d(side):aaf:ac:sleep
# W:lev:rarity:weight:xp:place('B'efore or 'A'fter)
# F:flags that the normal monster *must* have
# H:flags that the normal monster *must not* have
# M:monster flags that the ego-monster adds
# O:monster flags to remove (use MF_ALL for all)
# S:monster spells that the ego-monster adds
# T:monster spells to remove (use MF_ALL for all)

# +, -, %, =
def opcode_to_enum(opcode)
  case opcode
  when "+"
    return "EgoModType::add"
  when "-"
    return "EgoModType::sub"
  when "%"
    return "EgoModType::percent"
  when "="
    return "EgoModType::set"
  else
    puts "Invalid modifier opcode!"
    return "EgoModType::none"
  end
end


def gen_citr_var_name(egoname)
  return egoname.downcase.gsub(/[\(\)\-\s]/, '')
end

def gencode_cartesian_product(color, flags, var_name)
  code = ""
   flags.each do |f|
      if f =~ /R_CHAR_(.)/
        code << "  _cartesian[Tile('#{$1}', '#{color}')].push_back(#{var_name});\n"
      end
  end
  code << "\n"
  return code
end

 # 1. get all const_iterators:
 #
 #   ego_container_t::const_iterator <ego_name> = _container.find("<egoname>");
 #
 # 2. 
 #


def gencode_raceego_entries(fs_in, catalog_name, flags_list)
  code = ""
  citr_code = ""
  fs_in.rewind
  fs_in.each_line do |l|
    a = ""
    c = ""
    tome_ind = 0
    name = ""
    info_init = "{"
    xinfo_init = "{"
    #blow_init = "{"
    added_spell_init = "{SpellFreq::_none_, "
    added_spell_types = []
    removed_spell_types = []
    must_flags = []
    mustnot_flags = []
    added_flags = []
    removed_flags = []

    if l =~ /^N\:(\d+)\:(.+)/
      tome_ind = $1 
      name = $2.strip
      
      until ( (lsub = fs_in.gets) == "\n" )
        if lsub =~ /G\:(.)\:(.)/
          c = $1
          a = $2
          next
        end
        if lsub =~ /^I\:([\+|-|%|=])(\d+):([\+|-|%|=])(\d+)d([\+|-|%|=])(\d+):([\+|-|%|=])(\d+):([\+|-|%|=])(\d+):([\+|-|%|=])(\d+)/
          info_init << "EgoModifier(#{opcode_to_enum($1)}, #{$2}), EgoModifier(#{opcode_to_enum($3)}, #{$4})," 
          info_init << "EgoModifier(#{opcode_to_enum($5)}, #{$6}), EgoModifier(#{opcode_to_enum($7)}, #{$8})," 
          info_init << "EgoModifier(#{opcode_to_enum($9)}, #{$10}), EgoModifier(#{opcode_to_enum($11)}, #{$12})" 
          next
        end
        if lsub =~ /^W\:([\+|-|%|=])(\d+):(\d+):([\+|-|%|=])(\d+):([\+|-|%|=])(\d+):([A|B])/
          xinfo_init << "EgoModifier(#{opcode_to_enum($1)}, #{$2}), #{$3}, EgoModifier(#{opcode_to_enum($4)}, #{$5}),"
          xinfo_init << "EgoModifier(#{opcode_to_enum($6)}, #{$7}), \'#{$8}\'"
          next
        end
=begin
        if lsub =~ /^B\:(\w+):(\w+):([\+|-|%|=])(\d+)d([\+|-|%|=])(\d+)/
          if blow_init == "{"
            blow_init = "std::vector<RaceBlowMod>{"
          end
          blow_init << "{AttackMethod::#{$1.downcase}, AttackEffect::#{$2.downcase}, #{$3}, #{$4}},"
          next
        end
=end
        if lsub =~ /^F\:/
          must_flags << lsub.scan(/(\w+)[\s|\n]/)
          must_flags.flatten!
          next
        end
        if lsub =~ /^H\:/
          mustnot_flags << lsub.scan(/(\w+)[\s|\n]/)
          mustnot_flags.flatten!
          next
        end
        if lsub =~ /^O\:/
          removed_flags << lsub.scan(/(\w+)[\s|\n]/)
          removed_flags.flatten!
          next
        end
        if lsub =~ /^M\:/
          added_flags << lsub.scan(/(\w+)[\s|\n]/)
          added_flags.flatten!
          next
        end

        if lsub =~ /^S\:(1_IN_\d+)[\s|\n]/
          rest = $'
          added_spell_init = "{SpellFreq::#{make_valid_id($1)}, "
          added_spell_types << rest.scan(/(\w+)[\s|\n]/)
          next
        end
        if lsub =~  /^S\:\D/
          added_spell_types << lsub.scan(/(\w+)[\s|\n]/)
          next
        end
        if lsub =~ /^T\:/
          removed_spell_types << lsub.scan(/(\w+)[\s|\n]/)
          removed_spell_types.flatten!
          next
        end        
      end
      
      
     
      # Close open blocks.
      info_init << "}"
      #blow_init << "}"
      xinfo_init << "}"
      
      added_spell_types.flatten!
      added_spell_init << gencode_vector_flags("SpellType", added_spell_types) << "}"
      removed_spell_init = gencode_vector_flags("SpellType", removed_spell_types)

      # Create the init code for the different flags VECTORS.
      must_flags_init = gencode_vector_flags("RaceFlag", must_flags)
      mustnot_flags_init = gencode_vector_flags("RaceFlag", mustnot_flags)
      added_flags_init = gencode_vector_flags("RaceFlag", added_flags)
      removed_flags_init = gencode_vector_flags("RaceFlag", removed_flags)

      rego_info_init = "{ #{tome_ind}, #{gen_tile(c, a, "Tile")}, \"#{name}\", #{info_init}, #{xinfo_init}, "
      rego_info_init << "#{added_spell_init}, #{removed_spell_init}, "
      rego_info_init << "#{must_flags_init}, #{mustnot_flags_init}, #{added_flags_init}, #{removed_flags_init} }"
      #code << "  #{catalog_name}.insert(#{rego_info_init});\n"
      code << "  _container[\"#{name}\"] = #{rego_info_init};\n"
      var_name = gen_citr_var_name(name) + "_citr"
      code << "  ego_container_t::const_iterator #{var_name} = _container.find(\"#{name}\");\n"

      if c == "*" and a != "*"
        code << gencode_cartesian_product(a, must_flags, var_name)
      end
    end

  end
  return code

end

#Path to Tomenet.
game_path = ARGV[0]
fs_race = File.new(game_path + "/lib/game/r_info.txt", "r")
fs_race_ego = File.new(game_path + "/lib/game/re_info.txt", "r")
flags = scan_file(fs_race, /^F\:/, /(\w+)[\s|\n]/, true)
flags << scan_file(fs_race_ego, /^[F|H|M|O]\:/, /(\w+)[\s|\n]/, true)
flags.flatten!.uniq!.sort!
fs_race.close

#
# EgoRaceCatalog.cpp, Insert entries.
#
fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/CatalogEgoRace.skel.cpp", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/CatalogEgoRace.cpp", 'w')

fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Insert/
    fs_filled << gencode_raceego_entries(fs_race_ego, "tile_index", flags)
  end
end

fs_skel.close
fs_filled.close
fs_race_ego.close
