
#
# Following funcion is quite generic.
# 


=begin
indent = /[\s|\t]*/
id = /\w+/
type =/(?:#{id}#{indent})+[\*+|\s+]+/
var_declaration = /(#{type})(#{id})#{indent}[,|\)|;]/
func_declaration = /#{indent}#{id}?#{indent}(#{id})#{indent}(#{id})#{indent}\(/ # <rtype> <funcname> 
=end

#                                                                                     
# only to wrap TOMENET Send functions.
#
def wrap_send(fname_src, fname_dst)
  
  indent = /[\s|\t]*/
  id = /\w+/
  type =/(?:#{id}#{indent})+[\*+|\s+]+/
  var_declaration = /(#{type})(#{id})#{indent}[,|\)|;]/
  func_declaration = /^#{indent}#{id}?#{indent}(#{id})#{indent}(#{id})#{indent}\(/ # <rtype> <funcname>

  fsrc = File.new(fname_src, "r")
  fdst = File.new(fname_dst, "w")
  method_body = ""
  method_prototype = ""
  fsrc.each_line do |l|
    if func_declaration.match(l)
      puts l
      ret_type = $1
      func_name = $2
      args = $'.scan(var_declaration)
      if (l =~ /Send_(.+);/) 
        method_prototype = ret_type + " " + $1
        method_body = " { return " + func_name + "("
        if (args != [])
          args.each do |a|
            method_body << a[1] << ", "
          end
          method_body.chop!.chop!
        end
        method_body << "); }"
        fdst << method_prototype + method_body + "\n"
      end
    end
  end
  fsrc.close
  fdst.close
end

wrap_send(ARGV[0], ARGV[1])
