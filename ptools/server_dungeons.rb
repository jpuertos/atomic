def gencode_server_dungeons(fs_in, fs_out, mm_name)
  fs_in.each_line do |l|
    puts l
    x = 0
    y = 0
    dungeon_init = "{"
    #   IF RPG.
    #  if l =~ /\(\s*(\d+)\,\s*(\d+)\)\s((?:\w*|\s*)+)Lev\:\s+(\d+)\~\s*(\d+)\s+Req\:\s+(\d+)\s+type\:\s*(\d+)/ 
    if l =~ /\(\s*(\d+)\,\s*(\d+)\)\s\s((\w+|\s*)+)\s+Lev\:\s+(\d+)\-\s*(\d+)\s+Req\:\s+(\d+)/
      x = $1
      y = $2
      dungeon_init << "{#{x},#{y}}, \"#{$3.strip}\", #{$5}, #{$6}, #{$7}}"
      fs_out << "#{mm_name}.insert(#{dungeon_init});\n"
    end
  end
end

fs_input = File.new(ARGV[0], "r")
fs_output = File.new(ARGV[1], "w")

gencode_server_dungeons(fs_input, fs_output, "server_dungeons")

fs_input.close
fs_output.close
