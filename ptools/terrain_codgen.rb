require_relative 'codegen_common.rb'

#
# Generates calls to multimap.insert, in order to populate the multimap.
#
def gencode_terrain_entries(fs_in, container_name, flags_list, effects_list)
  code = ""
  fs_in.rewind
  fs_in.each_line do |l|
    tome_ind = 0
    desc = ""
    a = ""
    c = "" 
    mimic = -1 # -1 Means no mimic...
    flags = []
    dices = 0
    sides = 0
    rare = 0
    effect = "_none_" # If have not damage effect...
  
    if l =~ /^N\:(\d+)\:(.+)/
      tome_ind = $1 
      desc = $2.strip
      until ( (lsub = fs_in.gets) == "\n" )
        if lsub =~ /G\:(.)\:(\w)/
          c = $1
          a = $2
          next
        end
        if lsub =~ /^M\:/
          mimic = scan_line_fields(lsub, /^M\:(\d+)/)[0]
          next
        end
        if lsub =~ /^F\:/
          flags << lsub.scan(/(\w+)[\s|\n]/)
          flags.flatten!
          next
        end
        if lsub =~ /^E\:/ #WARNING: They say that up to 4 lines. But we only found 1.
          dices, sides, rare, effect = scan_line_fields(lsub, /^E:(\d+)d(\d+):(\d+):(\w+)/)
          next
        end      
      end       

      flags_init = gencode_flags_bitset_string("TerrainFlag", flags_list, flags)
      effect_init = "{TerrainEffectType::#{effect.downcase}, #{dices}, #{sides}, #{rare}}"
      tf_info_init = "{#{tome_ind}, #{gen_tile(c, a, "Tile")}, #{mimic}, \"#{desc}\", #{flags_init}, #{effect_init} }"
      code << "  #{container_name}.insert(#{tf_info_init});\n"
     end
  end
  return code
end

# Main...

#Path to Tomenet.
game_path = ARGV[0]
fs_terrain = File.new(game_path + "/lib/game/f_info.txt", "r")

flags = scan_file(fs_terrain, /^F\:/, /(\w+)[\s|\n]/, true)
effects = scan_file(fs_terrain, /^E\:/, /^E\:.+:.+:(\w+)$/, true)

fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/TerrainInfo.skel.h", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/TerrainInfo.h", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Enums/
    fs_filled << gencode_enum(flags, "TerrainFlag", true, true) << "\n"
    fs_filled << gencode_enum(effects, "TerrainEffectType", true, true) << "\n"
  end
end

fs_skel = File.new("#{game_path}/src/client/atomic/dictionary/CatalogTerrain.skel.cpp", 'r')
fs_filled = File.new("#{game_path}/src/client/atomic/dictionary/CatalogTerrain.cpp", 'w')
fs_skel.each_line do |l|
  fs_filled << l
  if l =~ /CODEGEN ENTRY: Insert/
    fs_filled << gencode_terrain_entries(fs_terrain, "tile_index", flags, effects)
  end
end

fs_skel.close
fs_filled.close
fs_terrain.close
