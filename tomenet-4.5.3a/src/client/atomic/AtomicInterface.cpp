/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "AtomicInterface.h"
#include "DummyBot.h" // FOR TTESTING SAVE REQUEST
#include <iostream>
#include <string.h>
#include <stdlib.h>

using namespace atomic;

AtomicInterface::AtomicInterface() {};

void AtomicInterface::get_message(char* msg)
{
  DummyBot bot;
  /* This should be passed to a parser... TODO */
  if ( strncmp(msg, "/a enable", 16) == 0) {
    enable();
    std::cout << "ATOMIC: Enabled." << std::endl;
  }

  if ( strncmp(msg, "/a disable", 16) == 0) {
    disable();
    std::cout << "ATOMIC: Disabled." << std::endl;
  }

  if ( strncmp(msg, "/a save", 16) == 0) {
    disable();
    bot.request_save(0);
    std::cout << "ATOMIC: Server state save requested." << std::endl;
  }

}

void AtomicInterface::disable(void)
{
  atomic_enabled = FALSE;
}

void AtomicInterface::enable(void)
{
  atomic_enabled = TRUE;
}

void AtomicInterface::update(void)
{
  bool got_frame = frame_grabber.commit();
  message_grabber.commit();

  scene_analizer.update(frame_grabber.get_history(),
			got_frame,
			message_grabber.get_history(),
			dummy_bot.get_upd_flags());

  dummy_bot.clear_updated_attr();
}

void AtomicInterface::grab_tixel(char x, char y, unsigned char a, char c) {
  frame_grabber.insert(Tixel(Coord2D(x, y), Tile(c, a)));
}

void AtomicInterface::grab_message(char* s)
{
  message_grabber.insert(std::string(s));
}


void  AtomicInterface::grab_updated_attr(const std::size_t& attr)
{
  dummy_bot.grab_updated_attr(attr);
}

void  AtomicInterface::grab_gold(const std::size_t& g, const std::size_t& b)
{
  dummy_bot.grab_gold(g, b);
}
void  AtomicInterface::grab_state(const bool& b1, const bool& b2, const bool& b3)
{
  dummy_bot.grab_state(b1, b2, b3);
}
void  AtomicInterface::grab_confused(const bool& b)
{
  dummy_bot.grab_confused(b);
}

void  AtomicInterface::grab_poison(const bool& b)
{
  dummy_bot.grab_poison(b);
}

void  AtomicInterface::grab_food(const std::size_t& f)
{
  dummy_bot.grab_food(f);
}

void  AtomicInterface::grab_fear(const bool& b)
{
  dummy_bot.grab_fear(b);
}

void  AtomicInterface::grab_speed(const std::size_t& s)
{
  dummy_bot.grab_speed(s);
}

void  AtomicInterface::grab_cut(const bool& b)
{
  dummy_bot.grab_cut(b);
}

void  AtomicInterface::grab_blind(const bool& b)
{
  dummy_bot.grab_blind(b);
}

void  AtomicInterface::grab_stun(const bool& b)
{
  dummy_bot.grab_stun(b);
}
