/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_INTERFACE__
#define __ATOMIC_INTERFACE__

#include "dictionary/Tile.h"
#include "MessageGrabber.h"
#include "FrameGrabber.h"
#include "SceneAnalizer.h"
#include "DummyBot.h"

class AtomicInterface {

public:
  AtomicInterface();
  void get_message(char* msg);
  void disable(void);
  void enable(void);
  void update(void);

  void grab_tixel(char, char, unsigned char, char);
  void count_tixel(void);
  void grab_message(char*);

  // DummyBot wrappers.
  void grab_updated_attr(const std::size_t&);
  void grab_gold(const std::size_t&, const std::size_t&);
  void grab_state(const bool&, const bool&, const bool&);
  void grab_confused(const bool&);
  void grab_poison(const bool&);
  void grab_food(const std::size_t&);
  void grab_fear(const bool&);
  void grab_speed(const std::size_t&);
  void grab_cut(const bool&);
  void grab_blind(const bool&);
  void grab_stun(const bool&);

private:
  atomic::SceneAnalizer scene_analizer;
  atomic::MessageGrabber message_grabber;
  atomic::FrameGrabber frame_grabber;
  atomic::DummyBot dummy_bot;
};

#endif
