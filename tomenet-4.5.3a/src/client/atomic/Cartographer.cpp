

#include <algorithm>

#include "Cartographer.h"

using namespace atomic;

Cartographer::Cartographer()
{
  map = new TileArray(boost::extents[MAX_HEIGHT][MAX_WIDTH]);

#warning WE MUST INIT MAP SOMEHOW BEFORE WE START ATOMIC.
#warning TODO: OR at least use std::fill
  // Here we should copy from scr, as soon as we enable ATOMIC.
  TileCache::iterator ins_it;
  ins_it = cache.insert(TileCachePair(Tile(' ', 'w'), catalog.find(Tile(' ', 'w')))).first;
  for (std::size_t i = 0; i < MAX_HEIGHT; i++) {
    for (std::size_t j = 0; j < MAX_WIDTH; j++) {
      (*map)[i][j] = ins_it;
    }
  }
}

void Cartographer::init(void)
{

}

Cartographer::Cartographer(const std::size_t& width, const std::size_t& height)
{
  map = new TileArray(boost::extents[height][width]);
}

/*
TileVariant Cartographer::operator[](const Coord2D& coord)
{

  OverlayTable<Mob>::const_iterator res_mob;
  res_mob = mob_layer.find(coord);
  if (res_mob != mob_layer.end())
    return res_mob->second.race;


  OverlayTable<Item>::const_iterator res_item;
  res_item = item_layer.find(coord);
  if (res_item != item_layer.end())
    return res_item->second.kind;


  OverlayTable<Trap>::const_iterator res_trap;
  trap_item = trap_layer.find(coord);
  if (res_trap != trap_layer.end())
    return res_trap->second.trap;

   // If nothing found in Layers, then return a terrain feature.

  return (*map_terrain)[coord.first][coord.second];
}
*/

/*
 * Returns a vector with the previos values of the Tixels that are modified,
 * in absolute coords, and updates the Map. 
 */
TixelEntryFrame Cartographer::update(const TixelFrame& frame, const GlobalCoord& gcoord)
{
  TixelEntryFrame diff;
  TileCache::iterator ins_it;
  Coord2D abs_coord;
  
  for(std::size_t i = 0; i < frame.size(); i++) {
    // If we have the same tile in the map, then we do nothing.
    abs_coord = absolute_coord(gcoord.sector, frame[i].first);    
    if((*map)[abs_coord.first][abs_coord.second]->first == frame[i].second)
      continue;

    ins_it = cache.find(frame[i].second);
    if (ins_it == cache.end())
      ins_it = cache.insert(TileCachePair(frame[i].second, catalog.find(frame[i].second))).first;

    // Debug code to show the actual frame update.
    std::cout << "> "<< abs_coord << ins_it->first << ": " << ins_it->second << std::endl;

    diff.push_back(TixelEntry(abs_coord, (*map)[abs_coord.first][abs_coord.second]));
    (*map)[abs_coord.first][abs_coord.second] = ins_it;
  }
  return diff;
  //TODO: Update the "memory map" and Terrain... and attempt to classify... and more, and more!
}

/*
 * Calcs the absolute coords, and return in row, col format.
 *
 */
Coord2D Cartographer::absolute_coord(const Coord2D& sector, const Coord2D& local)
{
  return Coord2D (  sector.second * PANEL_V_SCROLL + local.second - PANEL_Y, 
		    sector.first * PANEL_H_SCROLL + local.first - PANEL_X);
		  

		  
}

void Cartographer::clear_map(void)
{
  TileCache::iterator ins_it;
#warning TODO: Use std::fill
  ins_it = cache.insert(TileCachePair(Tile(' ', 'w'), catalog.find(Tile(' ', 'w')))).first;
  for (std::size_t i = 0; i < MAX_HEIGHT; i++) {
    for (std::size_t j = 0; j < MAX_WIDTH; j++) {
      (*map)[i][j] = ins_it;
    }
  }
}

void  Cartographer::clear_cache(void)
{
  cache.clear();
}

void Cartographer::print(void)
{
  for (std::size_t i = 0; i < MAX_HEIGHT; i++) {
    for (std::size_t j = 0; j < MAX_WIDTH; j++) {
      std::cout << (*map)[i][j]->first.first;
    }
    std::cout << std::endl;
  }
}

void Cartographer::print_cache(void)
{
  std::cout << cache;
}

std::ostream& operator<<(std::ostream& os, const atomic::TileCache& cache)
{
  atomic::TileCache::const_iterator it = cache.begin();
  for(it; it != cache.end(); it++)
    {
      os << it->first << ": " << it->second;
    }

  return os;
}
