





/*
 * Mobs, Items and Traps are represented in hash tables that overlay
 * respectively atop of the terrain. The overlay precedence is depicted here:
 *
 *         Mob
 *         Item
 *         Trap
 *     _________________
 *    / Terrain Feature \
 *
 * The map view is a merge of all different sectors.
 *
 *
 *
 * We will need to make use of boost::variants.
 * i.e. for implementing the operator[].
 * 
 */

#ifndef __ATOMIC_CARTO__
#define __ATOMIC_CARTO__

#undef bool
#include <boost/multi_array.hpp>
#include <boost/unordered_map.hpp>
#include <boost/variant.hpp>
#include <set>

#include "Coords.h"
#include "dictionary/TileCatalog.h"
#include "dictionary/DungeonCatalog.h"
#include "FrameGrabber.h"
#include "TileClassifier.h"
#include "Item.h"

namespace atomic {

  /* @common/defines.h and nclient.c */

  /* Game screen */
  const std::size_t SCREEN_WIDTH = 66;
  const std::size_t SCREEN_HEIGHT = 22;

  /* For the Big map option since 4.4.9b */
  const std::size_t SCREEN_BIG_HEIGHT = 44;

  /* Physical top-left screen position of view panel */
  const std::size_t PANEL_X = 13;
  const std::size_t PANEL_Y = 1;

  // Activates panel scroll.
  const std::size_t PANEL_V_SCROLL = 11; // +1 ?
  const std::size_t PANEL_H_SCROLL = 33; // +1 ?

  // Maximum dungeon dimension.
  const std::size_t MAX_WIDTH = SCREEN_WIDTH * 3;
  const std::size_t MAX_HEIGHT = SCREEN_HEIGHT * 3;

 /* Hash table of tiles (caches search results). */
  typedef boost::unordered_map<
    Tile,
    TileTuple
  > TileCache;

  typedef std::pair<Tile, TileTuple> TileCachePair;
  typedef boost::multi_array<TileCache::iterator, 2> TileArray;
  typedef boost::multi_array<Catalog<tf_info>::const_iterator, 2> TerrainArray;

  typedef std::pair<Coord2D, TileCache::iterator> TixelEntry;
  typedef std::vector<TixelEntry> TixelEntryFrame;

  /* Partially specialization, for an Overlay of entities present at this map.*/
  template <typename T>
  using OverlayTable = boost::unordered_map<Coord2D, T>;
  
  class Cartographer {
  public:
    Cartographer();
    Cartographer(const std::size_t&, const std::size_t&);
    void init(void);
    //TileVariant operator[](const Coord2D&);
    TixelEntryFrame update(const TixelFrame&, const GlobalCoord&);
    void print(void);
    void print_cache(void);
    void clear_map(void);
    void clear_cache(void);

  private:
    TileCatalog catalog;
    TileCache cache;
    TileArray* map;    // Does not 'erase' out of sight areas.
    std::set<Coord2D> out_sight; // Set of "out of sight areas".

    TerrainArray* map_terrain;  // Interpreted terrain, is an image of terrain features.

    //OverlayTable<Trap> trap_overlay;
    OverlayTable<Item> item_layer;
    //OverlayTable<Mob>  mob_layer;

    /* Helper to calc the absolute coords. */
    Coord2D absolute_coord(const Coord2D&, const Coord2D&);
  };
}

std::ostream& operator<<(std::ostream&, const atomic::TileArray&);
std::ostream& operator<<(std::ostream&, const atomic::TileCache&);

#endif
