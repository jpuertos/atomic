/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <algorithm>
#include <iomanip>
#include "Coords.h"

std::size_t distance(const Coord2D& a, const Coord2D& b) {
  return std::max(std::min(a.first - b.first, b.first - a.first),
		  std::min(a.second - b.second, b.second - a.second));
}

std::ostream& operator<<(std::ostream& os, const Coord2D& coord)
{
  os << std::setw(3) <<  coord.first << ", " << std::setw(3) << coord.second;
  return os;
}


std::ostream& operator<<(std::ostream& os, const GlobalCoord& loc) {
  os << "(" << loc.world
     << ")[" << loc.sector
     << "] " << loc.depth
     << " {" << loc.player
     << "}";
  return os;
}
