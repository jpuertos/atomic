/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_COORDS__
#define __ATOMIC_COORDS__

#include <iostream>


typedef std::pair<std::size_t, std::size_t> Coord2D;

std::size_t distance(const Coord2D&, const Coord2D&);

struct GlobalCoord {
  Coord2D world;  // Location in wilderness.
  Coord2D sector; // Sector.
  int depth;      // Depth, relative to ground level.
  Coord2D player; // @ location.
};

std::ostream& operator<<(std::ostream&, const Coord2D&);
std::ostream& operator<<(std::ostream&, const GlobalCoord&);

#endif
