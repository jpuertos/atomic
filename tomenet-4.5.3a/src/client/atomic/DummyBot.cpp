#include "DummyBot.h"
#include <iostream>

using namespace atomic;

DummyBot::DummyBot()
{

}

void DummyBot::grab_updated_attr(const std::size_t& attr)
{
  updated_attr.set(attr);
}

void DummyBot::grab_gold(const std::size_t& g, const std::size_t& b)
{
  gold = g;
  balance = b;
}

void DummyBot::grab_state(const bool& p, const bool& s, const bool& r)
{
  paralyzed = p;
  searching = s;
  resting = r;
}

void DummyBot::grab_confused(const bool& b)
{
  confused = b;
}

void DummyBot::grab_poison(const bool& b)
{
  poison = b;
}

void DummyBot::grab_food(const std::size_t& f)
{
  food = f;
}

void DummyBot::grab_fear(const bool& b)
{
  fear = b;
}

void DummyBot::grab_speed(const std::size_t& s)
{
  speed = s;
}

void DummyBot::grab_cut(const bool& b)
{
  cut = b;
}

void DummyBot::grab_blind(const bool& b)
{
  blind = b;
}

void DummyBot::grab_stun(const bool& b)
{
  stun = b;
}

void DummyBot::clear_updated_attr(void)
{
  updated_attr.reset();
}

PlayerUpdFlags& DummyBot::get_upd_flags(void)
{
  return updated_attr;
}
