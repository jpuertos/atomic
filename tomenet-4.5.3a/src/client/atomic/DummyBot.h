/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_DUMMY_BOT__
#define __ATOMIC_DUMMY_BOT__

#include <vector>
#include <string>
#include <bitset>

#include "import_from_tomenet.h"

namespace atomic {

  /*
   * Used to index the bitset that will hold what
   * has been attributes have updated during this frame.
   * We dont need to grab its values, cuz p_ptr is updated.
   */
  enum class PlayerUpdAttr {
    hp = 0,
    st,
    ac,
    mp,
    sp,
    xp,
    stat
  };

  typedef std::bitset<7> PlayerUpdFlags;

  /*
   * Holds state of vars that are not tracked at client's side.
   * wraps calles to player's actions.
   */
  class DummyBot {

  public:
    DummyBot();
    int file_check(int ind, unsigned short id, char *fname) { return Send_file_check(ind, id, fname); }
    int file_init(int ind, unsigned short id, char *fname) { return Send_file_init(ind, id, fname); }
    int file_data(int ind, unsigned short id, char *buf, unsigned short len) { return Send_file_data(ind, id, buf, len); }
    int file_end(int ind, unsigned short id) { return Send_file_end(ind, id); }
    int raw_key(int key) { return Send_raw_key(key); }
    int mimic(int spell) { return Send_mimic(spell); }
    int search(void) { return Send_search(); }
    int walk(int dir) { return Send_walk(dir); }
    int run(int dir) { return Send_run(dir); }
    int drop(int item, int amt) { return Send_drop(item, amt); }
    int drop_gold(s32b amt) { return Send_drop_gold(amt); }
    int tunnel(int dir) { return Send_tunnel(dir); }
    int stay(void) { return Send_stay(); }
    int toggle_search(void) { return Send_toggle_search(); }
    int rest(void) { return Send_rest(); }
    int go_up(void) { return Send_go_up(); }
    int go_down(void) { return Send_go_down(); }
    int open(int dir) { return Send_open(dir); }
    int close(int dir) { return Send_close(dir); }
    int bash(int dir) { return Send_bash(dir); }
    int disarm(int dir) { return Send_disarm(dir); }
    int wield(int item) { return Send_wield(item); }
    int observe(int item) { return Send_observe(item); }
    int take_off(int item) { return Send_take_off(item); }
    int destroy(int item, int amt) { return Send_destroy(item, amt); }
    int inscribe(int item, cptr buf) { return Send_inscribe(item, buf); }
    int uninscribe(int item) { return Send_uninscribe(item); }
    int steal(int dir) { return Send_steal(dir); }
    int quaff(int item) { return Send_quaff(item); }
    int read(int item) { return Send_read(item); }
    int aim(int item, int dir) { return Send_aim(item, dir); }
    int use(int item) { return Send_use(item); }
    int zap(int item) { return Send_zap(item); }
    int zap_dir(int item, int dir) { return Send_zap_dir(item, dir); }
    int fill(int item) { return Send_fill(item); }
    int eat(int item) { return Send_eat(item); }
    int activate(int item) { return Send_activate(item); }
    int activate_dir(int item, int dir) { return Send_activate_dir(item, dir); }
    int target(int dir) { return Send_target(dir); }
    int target_friendly(int dir) { return Send_target_friendly(dir); }
    int look(int dir) { return Send_look(dir); }
    int msg(cptr message) { return Send_msg(message); }
    int fire(int dir) { return Send_fire(dir); }
    int throw_(int item, int dir) { return Send_throw(item, dir); }
    int item(int item) { return Send_item(item); }
    int gain(int book, int spell) { return Send_gain(book, spell); }
    int cast(int book, int spell) { return Send_cast(book, spell); }
    int pray(int book, int spell) { return Send_pray(book, spell); }
    int fight(int book, int spell) { return Send_fight(book, spell); }
    int ghost(int ability) { return Send_ghost(ability); }
    int map(char mode) { return Send_map(mode); }
    int locate(int dir) { return Send_locate(dir); }
    int store_purchase(int item, int amt) { return Send_store_purchase(item, amt); }
    int store_sell(int item, int amt) { return Send_store_sell(item, amt); }
    int store_leave(void) { return Send_store_leave(); }
    int store_confirm(void) { return Send_store_confirm(); }
    int redraw(char mode) { return Send_redraw(mode); }
    int special_line(int type, int line) { return Send_special_line(type, line); }
    int party(s16b command, cptr buf) { return Send_party(command, buf); }
    int guild(s16b command, cptr buf) { return Send_guild(command, buf); }
    int purchase_house(int dir) { return Send_purchase_house(dir); }
    int suicide(void) { return Send_suicide(); }
    int options(void) { return Send_options(); }
    int master(s16b command, cptr buf) { return Send_master(command, buf); }
    int clear_buffer(void) { return Send_clear_buffer(); }
    int clear_actions(void) { return Send_clear_actions(); }
    int King(byte type) { return Send_King(type); }
    int force_stack(int item) { return Send_force_stack(item); }
    int admin_house(int dir, cptr buf) { return Send_admin_house(dir, buf); }
    int spike(int dir) { return Send_spike(dir); }
    int skill_mod(int i) { return Send_skill_mod(i); }
    int store_examine(int item) { return Send_store_examine(item); }
    int store_command(int action, int item, int item2, int amt, int gold) { return Send_store_command(action, item, item2, amt, gold); }
    int activate_skill(int mkey, int book, int spell, int dir, int item, int aux) { return Send_activate_skill(mkey, book, spell, dir, item, aux); }
    int ping(void) { return Send_ping(); }
    int sip(void) { return Send_sip(); }
    int telekinesis(void) { return Send_telekinesis(); }
    int BBS(void) { return Send_BBS(); }
    int wield2(int item) { return Send_wield2(item); }
    int cloak(void) { return Send_cloak(); }
    int inventory_revision(int revision) { return Send_inventory_revision(revision); }
    int account_info(void) { return Send_account_info(); }
    int change_password(char *old_pass, char *new_pass) { return Send_change_password(old_pass, new_pass); }
    int request_key(int id, char key) { return Send_request_key(id, key); }
    int request_num(int id, int num) { return Send_request_num(id, num); }
    int request_str(int id, char *str) { return Send_request_str(id, str); }
    int request_cfr(int id, int cfr) { return Send_request_cfr(id, cfr); }
    int request_save(int id) { return Send_request_save(id); }
    
    void grab_updated_attr(const std::size_t&);
    void grab_gold(const std::size_t&, const std::size_t&);
    void grab_state(const bool&, const bool&, const bool&);
    void grab_confused(const bool&);
    void grab_poison(const bool&);
    void grab_food(const std::size_t&);
    void grab_fear(const bool&);
    void grab_speed(const std::size_t&);
    void grab_cut(const bool&);
    void grab_blind(const bool&);
    void grab_stun(const bool&);
    void clear_updated_attr(void);
    PlayerUpdFlags& get_upd_flags(void);

  private:
    PlayerUpdFlags updated_attr;
    bool paralyzed;
    bool searching;
    bool resting;
    bool confused;
    bool poison;
    std::size_t food;
    bool fear;
    std::size_t speed;
    bool cut;
    bool blind;
    bool stun;
    std::size_t gold;
    std::size_t balance;
  };

}

#endif
