/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_FLAGS_TESTER__
#define __ATOMIC_FLAGS_TESTER__

#include <bitset>
#include <vector>

template<size_t N, typename T>
bool test_flags_any(const std::bitset<N>& bs, const std::vector<T>& flags) {
  for(int i = 0; i < flags.size(); i++) {
    if (bs.test((size_t) flags[i]))
      return true;
  }
  return false;
}

template<size_t N, typename T>
bool test_flags_all(const std::bitset<N>& bs, const std::vector<T>& flags) {
  for(int i = 0; i < flags.size(); i++) {
    if (!bs.test((size_t) flags[i]))
      return false;
  }
  return true;
}

#endif
