#include "FrameGrabber.h"

using namespace atomic;

FrameGrabber::FrameGrabber()
{
  frame = {};
}

void FrameGrabber::insert(const Tixel& tix)
{
  frame.push_back(tix);
}

bool FrameGrabber::commit(void)
{
  /*
   * We drop frames that only ontain player pos.
   * Sometimes we get 2 times the player pos.
   */
  if (frame.size() > 1)
    {
    if (frame[0].first != frame[1].first) // SEGFAULT.
      {     
	frame_history.push_back(frame);
	frame.clear();
	return true;
      }
    }

  frame.clear();
  return false;
}

void FrameGrabber::clear_history(void)
{
  frame_history.clear();
  frame.clear();
}

void FrameGrabber::clear_all(void)
{
  clear_history();
}

std::vector<TixelFrame>& FrameGrabber::get_history(void)
{
  return frame_history;
}
