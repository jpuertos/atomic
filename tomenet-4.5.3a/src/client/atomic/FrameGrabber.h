

#ifndef __ATOMIC_FRAME_GRABBER__
#define __ATOMIC_FRAME_GRABBER__

#undef bool
#include <boost/unordered_map.hpp>
#include <vector>
#include "Coords.h"
#include "dictionary/Tile.h"

namespace atomic {

  /* Unprocessed Tixels.
   * Coords should be in absolute value.
   */
  typedef std::pair<Coord2D, Tile> Tixel;
  typedef std::vector<Tixel> TixelFrame;
  
  class FrameGrabber {
  public:
    FrameGrabber();
    void insert(const Tixel&);
    bool commit(void);
    void clear_history(void);
    void clear_all(void);
    std::vector<TixelFrame>& get_history(void);

  private:
    TixelFrame frame; // Still unprocessed vector of tixels.
    std::vector<TixelFrame> frame_history;
  };
}

#endif
