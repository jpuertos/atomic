#include "MessageGrabber.h"

using namespace atomic;

MessageGrabber::MessageGrabber()
{
}

void MessageGrabber::insert(const std::string& s)
{
  // Remove color codes;
  std::string ss = s; 
  std::size_t pos = ss.find(COLOR_CODE);
  while( pos != std::string::npos) {
    ss.erase(pos, 2);
    pos = ss.find(COLOR_CODE);
  }

  current_frame.push_back(ss);
}

void MessageGrabber::commit(void)
{
  msgs_history.push_back(current_frame);
  current_frame.clear();
}

std::vector<MsgFrame>& MessageGrabber::get_history(void)
{
  return msgs_history;
}
