
#ifndef __ATOMIC_MESSAGE_GRABBER__
#define __ATOMIC_MESSAGE_GRABBER__

#include <vector>
#include <string>

namespace atomic {

  const char COLOR_CODE = '\377';

  typedef std::vector<std::string> MsgFrame;

  class MessageGrabber {
  public:
    MessageGrabber();
    void insert(const std::string&);
    void commit(void);
    std::vector<MsgFrame>& get_history(void);

  private:
    MsgFrame current_frame;
    std::vector<MsgFrame> msgs_history;
  };

}

#endif
