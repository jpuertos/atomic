#include <boost/regex.hpp>
#include "MessageParser.h"

using namespace atomic;

MessageParser::MessageParser() {
  action = std::vector<EventRegexp>();
  action.push_back(EventRegexp(EventMsg::enter, std::string("^You enter (.*)\\.\\.")));
  action.push_back(EventRegexp(EventMsg::transported_into, std::string("^You are transported into (.*)\\.\\.")));
  action.push_back(EventRegexp(EventMsg::leave, std::string("^You leave (.*)\\.\\.")));
  action.push_back(EventRegexp(EventMsg::transported_out, std::string("^You are transported out of (.*)\\.\\.")));
  action.push_back(EventRegexp(EventMsg::enter_next, std::string("^You enter the next area\\.")));
  action.push_back(EventRegexp(EventMsg::enter_down, std::string("^You enter a maze of down staircases\\.")));
  action.push_back(EventRegexp(EventMsg::enter_up, std::string("^You enter a maze of up staircases\\.")));
  action.push_back(EventRegexp(EventMsg::enter_previous, std::string("^You enter the previous area\\.")));
  action.push_back(EventRegexp(EventMsg::hit, std::string("^You hit the (.*) for (\\d+) damage\\.")));
  action.push_back(EventRegexp(EventMsg::miss, std::string("^You miss the (.*)\\.")));
  action.push_back(EventRegexp(EventMsg::attacks, std::string("(\\w|\\s)+hits you\\sfor(\\d+)damage\\.")));
  action.push_back(EventRegexp(EventMsg::map_sector, std::string("^Map sector \\[(\\d),(\\d).*")));
  action.push_back(EventRegexp(EventMsg::fall_void, std::string("^You fall into the void\\.")));
  action.push_back(EventRegexp(EventMsg::yanked_downwards, std::string("^You feel yourself yanked downwards!")));
  action.push_back(EventRegexp(EventMsg::yanked_upwards, std::string("^You feel yourself yanked upwards!")));
}

EventTokens MessageParser::parse(const std::string& text) { 
  boost::smatch matches;
  EventTokens et(EventMsg::none, Token("",0));
  for(int i = 0; i < action.size(); i++) {
    if(boost::regex_match(text, matches, boost::regex(action[i].second))) {
      et.first = action[i].first;
      switch (action[i].first)
	{
	case EventMsg::enter:
	case EventMsg::leave:
	  et.second = Token(std::string(matches[1].first, matches[1].second), 0);
	  break;

	case EventMsg::hit:
	  et.second = Token(std::string(matches[1].first, matches[1].second),
			    std::stoi(std::string(matches[2].first, matches[2].second)));
	  break;

	case EventMsg::miss:
	  et.second = Token(std::string(matches[1].first, matches[1].second), 0);
	  break;

	  /*  This is certanly a hack.
	   *    and remember that sector locator gives in [Y, X]
	   *    so here we put it like sc = X*10 + Y
	   */
	case EventMsg::map_sector:
	  et.second =  Token(std::string(""),
			     std::stoi(std::string(matches[2].first, matches[2].second))*10
			     + std::stoi(std::string(matches[1].first, matches[1].second)));

	  break;
	}
      break;
    }
  }
  return et;
}
