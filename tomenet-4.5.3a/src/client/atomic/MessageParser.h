

#ifndef __ATOMIC_MESSAGE_PARSER__
#define __ATOMIC_MESSAGE_PARSER__

#include <boost/tuple/tuple.hpp>
#include <boost/variant.hpp>

#include <vector>
#include <string>
#include <iostream>

namespace atomic {

  enum class EventMsg {
    none,

      //Performed by us.
      hit,     // We hit a mob      (what, points)
      miss,    // we miss           (what)  
      kill,    // We destroy a mob. (what)
      enter,   // We enter a dung.  (name)
      transported_into,
      leave,   // We leave a dung.
      transported_out,
      die,
      map_sector,
      fall_void,
      yanked_downwards,
      yanked_upwards,
      enter_next,
      enter_previous,
      enter_up,
      enter_down,

      // Performed by mobs.
      attacks,     // (who, points)
      misses,   // (who)
      wakes_up,
      flees,
      dies,
      };

  /*
   *  A:     You perform an action A.
   *  A, V:  You perform an action A with V.
   *  A, S:   S performs action A.
   *  A, S, V  S performs action A with V.
   */


  /* //For Feelings:
   * http://koti.mbnet.fi/mikaelh/tomenet/guide.php?chapter=4.8
   */

  typedef std::pair<EventMsg, std::string > EventRegexp;
  typedef std::pair<std::string, std::size_t> Token;
  typedef std::pair<EventMsg, Token> EventTokens;
    
  class MessageParser {
  public:
    MessageParser();
    EventTokens parse(const std::string&);
      
  private:
    std::vector<EventRegexp> action;
  }; 
}
#endif
