/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <iostream>
#include <iomanip>
#include <math.h>
#include <cstring>
#include "SceneAnalizer.h"

using namespace atomic;

SceneAnalizer::SceneAnalizer() {
  avatar = {'@', 1};
  loc = { {0, 0}, {0, 0}, -50000, {0, 0}}; //Will force a world scroll.
  area = AreaType::wilderness;
  dungeon = dungeon_enc.find("Wilderness");
}

void SceneAnalizer::ask_sector_location(const std::size_t& t) {
  sector_coords_req_timeout = t;
  sector_coords_req_tick = ticks;
  sector_locator.ask_sector_location();
}

void SceneAnalizer::update(std::vector<TixelFrame>& frame_history,
			   bool& got_frame,
			   std::vector<MsgFrame>& msg_history,
			   const PlayerUpdFlags& upd_flags)
{
  /*
   * We always read world coords and depth, client's got it.
   * if we are in a new world location we perform a
   * sector locate request. NOTICE that we don't set player coords.
   * Then  If we have changed our World location we must perform a query.
   */

  bool got_coords = false;
  GlobalCoord new_loc;
  read_world_coords(new_loc);
  new_loc.sector = loc.sector;
  new_loc.player = loc.player;

  /*
   * Should we ask for map sector depending on our new pos? 
   * This could be bad, we skip Msgs processin.
   */
  if (loc.world != new_loc.world)
    {
      loc = new_loc;
      ask_sector_location(10);
      map.clear_cache();
      map.clear_map();
      return;
    }

  /*
   * We parse Messages and perform semantic actions
   * such as looking up for a Dungeon.
   */

  
  EventTokens et;
  for(int i = 0; i < msg_history[msg_history.size()-1].size(); i++) {
    std::cout << msg_history[msg_history.size()-1][i] << std::endl;
    et = msg_parser.parse(msg_history[msg_history.size()-1][i]);
    switch (et.first) {

    case EventMsg::enter:
    case EventMsg::transported_into:
      area = AreaType::dungeon; // What if it is a Tower?
      dungeon = dungeon_enc.find(et.second.first);
      loc = new_loc;
      ask_sector_location(10);
      map.clear_cache();
      map.clear_map();
      return;

    case EventMsg::leave:
    case EventMsg::transported_out:
      loc = new_loc;
      area = AreaType::wilderness; // But what type?
      ask_sector_location(10);
      map.clear_cache();
      map.clear_map();
      return;

    case EventMsg::enter_next:
    case EventMsg::enter_previous:
    case EventMsg::enter_up:
    case EventMsg::enter_down:
      loc = new_loc;
      ask_sector_location(10);
      map.clear_map();
      return;

    case EventMsg::fall_void:
      loc = new_loc;
      ask_sector_location(10);
      return;

    case EventMsg::hit:
      break;

    case EventMsg::miss:
      break;
      
    case EventMsg::map_sector:
      got_coords = true;
      sector_locator.location_received();
      new_loc.sector = Coord2D(et.second.second / 10, et.second.second % 10); // Y
      break;

    default:
      break;
    }
  }

  // If we recieve a Full screen and there is no other reason for this,
  // Then it means that we have entered another sector.

  if (!got_coords && got_frame
      && frame_history[frame_history.size()-1].size() >= SCREEN_WIDTH * SCREEN_HEIGHT)
    {
      loc = new_loc;
      ask_sector_location(10);
      return;
    }

  /*
   * If we are still waiting for the sector coordinates.
   *   1. If dont have coords and time outs, we ask for them again.
   *   2. but if no timeout yet, we just return.
   *   3. If we have not requested any sector map coords, we inherit the previous sector cords.
   */
  if (sector_locator.requested())
    {
      if (!got_coords)
	{
	  if (ticks - sector_coords_req_tick > sector_coords_req_timeout)
	    {
	      sector_locator.location_received(); // Clean up the timedout request.
	      ask_sector_location(10);
	      return;
	    }
	  return;
	}
    }

 /*
   *  Check for Player internal state updates from server.
   *
   *  1. if WE receive a Health packet, then we much check how do we look like.
   */
  if (upd_flags[std::size_t(PlayerUpdAttr::hp)])
    if (update_avatar())
      std::cout << "avatar: " << avatar << std::endl;

  /*
   * Atempt to classify what we see.
   * We only print out for now the tiles we have recieved.
   */

  // OJO will display PREVIOUS value.
  TixelEntryFrame diff;
  diff = map.update(frame_history[frame_history.size()-1], new_loc);
  if (got_frame && diff.size() > 1)
    {
      for (int i = 0; i < diff.size(); i++)
	{
	  //tile_classifier.filter( diff[i].second->second, dungeon, new_loc.depth );
	  std::cout << diff[i].first << ": " << diff[i].second->first << diff[i].second->second << std::endl;
	}
    }

  TixelFrame tf;
  tf = frame_history[frame_history.size()-1];
  if (got_frame)
    {
      //std::cout << "lfs: " << tf.size() << " diff: " << diff.size() << std::endl;
      for (int i = 0; i < tf.size(); i++) {
	if (tf[i].second == avatar)
	  {
	    new_loc.player = tf[i].first;
	    //std::cout << new_loc << std::endl;
	    if (tf.size() > 20)
	      {
		map.print();
		map.print_cache();
	      }
	  }
      }
    }
  loc = new_loc;

   // Debug: Messages received this frame.
  
  if (msg_history[msg_history.size()-1].size() > 0 || (got_frame && diff.size() > 1) )
    std::cout << "*********************************************************************************" << std::endl;

  return;
}

void SceneAnalizer::read_world_coords(GlobalCoord& loc)
{
  loc.world.first = p_ptr->wpos.wx;
  loc.world.second = p_ptr->wpos.wy;
  loc.depth = p_ptr->wpos.wz;
}

bool SceneAnalizer::panel_scroll(const Coord2D& prev, const Coord2D& cur)
{
  if (std::min(cur.first - prev.first, prev.first - cur.first) +1 == PANEL_H_SCROLL)
    return true;

  if (std::min(cur.second - prev.second, prev.second - cur.second) +1 == PANEL_V_SCROLL)
    return true;
  
  return false;
}

/*
 *  This is still naive, does not consider different forms. We can certanly code this
 *  if we take a look at caves.c:3913
 *
 *   Still have to code the "player color".
 */
bool SceneAnalizer::update_avatar(void)
{
  const std::size_t TURN_CHAR_INTO_NUMBE = 7;
  bool changes = false;
  int num = (p_ptr->chp * 95) / (p_ptr->mhp * 10); // @cave.c:3927
  if  ( num > TURN_CHAR_INTO_NUMBER )
    {
      if (avatar.first != '@')
	changes = true;
      avatar.first = '@';
    }
  else
    {
      if (avatar.first != '0' + num)
	changes = true;
      avatar.first = '0' + num;
    }

  return changes;
}
