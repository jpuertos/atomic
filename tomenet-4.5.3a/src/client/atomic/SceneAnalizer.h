/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_SCENEANALIZER__
#define __ATOMIC_SCENEANALIZER__

#include <boost/multi_array.hpp>
#include <vector>

#include "ScreenReader.h"
#include "MessageParser.h"
#include "MessageGrabber.h"
#include "FrameGrabber.h"
#include "DummyBot.h"
#include "Cartographer.h"
#include "SectorLocator.h"
#include "TileClassifier.h"
#include "dictionary/DungeonCatalog.h"


/*
 * High-level view of the dungeon, town or whatever...
 **/

namespace atomic {
  
  enum class AreaType {
      wilderness,
      town,
      dungeon,
      tower
  };
  
  
  class SceneAnalizer {
  public:
    SceneAnalizer();
    void update(std::vector<TixelFrame>&,
		bool&,
		std::vector<MsgFrame>&,
		const PlayerUpdFlags&);

    void update_current_sector(void);
    void ask_sector_location(const std::size_t&);
    bool read_sector_location(void);

  private:
    void read_world_coords(GlobalCoord&);
    bool panel_scroll(const Coord2D&, const Coord2D&);
    bool change_area_type(const GlobalCoord&, const GlobalCoord&);

    void classify_frame_entities(const TixelFrame& frame,
				 const TixelFrame& diff_frame,
				 const GlobalCoord& gcoord);  // <- This is filtering ans classifying what we see.
                                                              //    based on the context, and messages received.
 
  private:
    // ScreenReader screen;
    MessageParser msg_parser;
    TileClassifier tile_classifier;
    DungeonCatalog dungeon_enc;
    SectorLocator sector_locator;
    Cartographer map;

    std::size_t sector_coords_req_tick;
    std::size_t sector_coords_req_timeout;

    Tile avatar;
    GlobalCoord loc;
    AreaType area;
    Dungeon dungeon;

    bool update_avatar(void);
    //  FeelingType feeling;
    bool next_has_feelining;
  };

}

#endif
