/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/



#include <iostream>
#include <stdlib.h>
#include <cctype>

#include "ScreenReader.h"

using namespace atomic;

//ctor-initializer... Alows us to init references.
ScreenReader::ScreenReader() : scr(ang_term[0]->scr),
			       message_bar(ang_term[0]->scr->c[0], ang_term[0]->scr->a[0]),
			       status_bar(ang_term[0]->scr->c[23], ang_term[0]->scr->a[23]),
			       world_coords(ang_term[0]->scr->c[21], ang_term[0]->scr->a[21]),
			       lagometer(ang_term[0]->scr->c[8], ang_term[0]->scr->a[8])
{

}

MessageLine ScreenReader::read_message_bar(void) {
  MessageLine m = { MessageType::none, std::string(message_bar.first, 80) };
  return m;
}
