/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_SCREEN_READER__
#define __ATOMIC_SCREEN_READER__

#include <string>
#include "Coords.h"

#include "import_from_tomenet.h"

namespace atomic {

  typedef std::pair<char* const &, unsigned char* const &> LinePtr;

  // Adjust this to maze view.
  //  const int SCREEN_WIDTH = 80;
  // const int SCREEN_HEIGHT = 24;

 
  enum class MessageType {
    none,
      game,
      info,
      party,
      pub, //Stands for 'public'.
      pri, // private.
      };

  struct MessageLine {
    MessageType type;
    std::string text;
  };

  /*
    enum class FeelingType = {
    imminent_danger,
    distorted,
    suppressive,
    lose_direction,
    stranger,
    solid,
    ,
    no_recall,
    };

    enum class ExtraFeelingType = {
    };
  */

  class ScreenReader {
  public:
    ScreenReader();
    MessageLine read_message_bar(void);
    int read_lagometer(void);

  private:
    term_win* const & scr;
    LinePtr message_bar;
    LinePtr status_bar;
    LinePtr world_coords;
    LinePtr lagometer;
  };

}

#endif
