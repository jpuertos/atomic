/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "SectorLocator.h"

using namespace atomic;
SectorLocator::SectorLocator() {
  locate_mode = false;
  locate_requested = false; 
  locate_dir = Direction::none;
}

void SectorLocator::enter_locate_mode()
{
  if (!locate_mode) {
    Send_locate(int (Direction::none)); // To enter "locate mode" at server side.
    locate_mode = true;
  }
}

void SectorLocator::exit_locate_mode()
{
  if (locate_mode) {
    Send_locate(0);     // To exit "locate mode".
    locate_mode = false;
  }
}

// return bool?
void SectorLocator::request_sector(Direction dir) {
  if ( locate_mode && !locate_requested) {
    Send_locate(int(dir));
    locate_requested = true;
    locate_dir = dir;
  } 
}

void SectorLocator::ask_sector_location(void) {
  if (!locate_mode && !locate_requested) {
    Send_locate(int(Direction::none)); // To enter the "locate mode".
    Send_locate(0);                    // To exit "locate mode".
    locate_requested = true;
    locate_dir = Direction::none;
  }
}

void SectorLocator::location_received(void) {
  locate_requested = false;
}

// Some functions missing? For cleanning up when all done?
bool SectorLocator::requested(void) {
  return locate_requested;
}
