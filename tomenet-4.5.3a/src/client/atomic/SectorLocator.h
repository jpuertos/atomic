/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef __ATOMIC_SECTOR_LOCATOR__
#define __ATOMIC_SECTOR_LOCATOR__

#include "import_from_tomenet.h"

namespace atomic {

  enum class Direction {
    north  = 8,
    northeast = 9,
    east  = 6,
    southeast = 3,
    south  = 2,
    southwest = 1,
    west  = 4,
    none = 5,
      };

  class SectorLocator {
  public:
    SectorLocator();
    void enter_locate_mode(void);
    void exit_locate_mode(void);
    void request_sector(Direction); // Emulates the 'L' + dir Send_locate(5);
    void ask_sector_location();
    void location_received(void);
    bool requested(void);

  private:
    bool locate_mode;      // Are we in locate mode? 'L'.  
    bool locate_requested; // Have we issued a request?
    Direction locate_dir;  // Where do we want to locate at.
  };
}

#endif
