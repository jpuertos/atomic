#include <limits>
#include "TileClassifier.h"

using namespace atomic;

TileClassifier::TileClassifier()
{
}

//TileClassifier::apply_egorace(TileTuple&)


/*
// Given context.

void TileClassifier::filter_monster(TileTuple& t, const Dungeon& d, const int& depth) {
  
  int dlevel = (d.first)->start_level + abs(depth);
  std::list< RaceContainer::nth_index<1>::type::const_iterator >::iterator it_race = t.get<0>().begin();
  for (it_race; it_race != t.get<0>().end(); it_race++)
    {
      // Only apears at higher levels than this.
      if((*it_race)->flags[std::size_t(RaceFlag::force_depth)]
	 && (*it_race)->xinfo.depth > dlevel)
	{
	  it_race = t.get<0>().erase(it_race);
	  continue;
	}

      // Only at one level.
      if((*it_race)->flags[std::size_t(RaceFlag::only_depth)]
	 && (*it_race)->xinfo.depth != dlevel)
	{
	  it_race = t.get<0>().erase(it_race);
	  continue;
	}

      // Too out-of-depth
      if((*it_race)->xinfo.depth > dlevel + 40) {
	it_race = t.get<0>().erase(it_race);
      }
    }
}


// Picks the monster closest to our currnet level.
RaceContainer::nth_index<1>::type::const_iterator TileClassifier::pick_monster(TileTuple& t, const Dungeon& d, const int& depth)
{
  RaceContainer::nth_index<1>::type::const_iterator m;
  std::size_t dist = std::numeric_limits<std::size_t>::max();
  std::size_t tmp_dist = 0;
  std::size_t dlevel = (d.first)->start_level + abs(depth);
  std::list< RaceContainer::nth_index<1>::type::const_iterator >::iterator it_race = t.get<0>().begin();
  for (it_race; it_race != t.get<0>().end(); it_race++)
    {
      tmp_dist = abs(dlevel - (*it_race)->xinfo.depth);
      if (tmp_dist < dist)
	{
	  dist = tmp_dist;
	  m = (*it_race);
	}
    }
  return m;
}

void TileClassifier::filter_feature(TileTuple& t, const Dungeon& d, const int& depth)
{
  //Discard terrain features that are not present at current level.
  std::list< TerrainContainer::nth_index<1>::type::const_iterator >::iterator it_terr = t.get<1>().begin();
  for (it_terr; it_terr != t.get<1>().end(); it_terr++)
    {
      if (!is_feature((d.second)->floor, (*it_terr)->tome_ind)
	  && !is_feature( (d.second)->wall, (*it_terr)->tome_ind))
	{
	  it_terr = t.get<1>().erase(it_terr);
	}
    }
}

void TileClassifier::filter(TileTuple& t, const Dungeon& d, const int& depth)
{
  if (t.get<0>().size() > 0)
    filter_monster(t, d, depth);
  
  if (t.get<1>().size() > 0)
    filter_feature(t, d, depth);
}


bool TileClassifier::is_feature(const FeatureList& fl, const int& tome_ind)
{
  for (int i = 0; i < fl.size(); i++) {
    if (fl[i].first == tome_ind)
      return true;
  }
  return false;
}
*/
