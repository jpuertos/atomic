/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_TILE_CLASSIFIER__
#define __ATOMIC_TILE_CLASSIFIER__

#include <boost/variant.hpp>
#include "dictionary/TileCatalog.h"
#include "dictionary/DungeonCatalog.h"

namespace atomic {
  
  typedef std::pair<
    Catalog<r_info>::const_iterator,
    Catalog<re_info>::const_iterator
  > QualifiedRace;

  typedef boost::tuple<
    QualifiedRace,
    Catalog<tf_info>::const_iterator,
    Catalog<k_info>::const_iterator
  > TileVariant;

  class TileClassifier {
  public:
    TileClassifier();
    TileVariant classify(TileTuple&, const Dungeon&, const int&); // Given context.

  private:
    //void apply_egorace(TileTuple&);
    QualifiedRace pick_monster(TileTuple&, const Dungeon&, const int&); // Given context.
    bool is_feature(const FeatureList&, const int&);
    void filter_feature(TileTuple&, const Dungeon&, const int&); // Given context.
  };
}

#endif
