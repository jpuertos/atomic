/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "astar_maze.h"


void AStar_maze::set_grid(const grid&)
{
  m_grid = grid;
}

void AStar_maze::set_source(const vertex_descriptor& source)
{
  s = source;
}

void AStar_maze::set_goal(const vertex_descriptor& goal)
{
  goal = g;
}

vertex_descriptor AStar_maze::source() const
{
  return s;
}

vertex_descriptor AStar_maze::goal() const
{
  return g;
}

bool AStar_maze::solve()
{
  euclidean_heuristic heuristic(g);
  astar_goal_visitor visitor(g);

  astar_search(m_barrier_grid, s, heuristic,
	       boost::weight_map(weight).
	       predecessor_map(pred_pmap).
	       distance_map(dist_pmap).
	       visitor(visitor) );

  // Walk backwards from the goal through the predecessor chain adding
  // vertices to the solution path.
  for (vertex_descriptor u = g; u != s; u = predecessor[u])
    m_solution.insert(u);
  
  m_solution.insert(s);
  m_solution_length = distance[g];
  return true;
}

  return false;
}

bool AStar_maze::solved() const
{
  return !m_solution.empty();
}

bool AStar_maze::solution_contains(vertex_descriptor u) const
{
  return m_solution.find(u) != m_solution.end();
}

filtered_grid AStar_maze::create_barrier_grid()
{
  return boost::make_vertex_subset_complement_filter(m_grid, m_barriers);
}


