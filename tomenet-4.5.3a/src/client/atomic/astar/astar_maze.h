/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "astar_heuristic.h"

class AStar_maze {
public:
  astar_maze();
  astar_maze(const grid&);
  void set_grid(const grid&);
  void set_source(const vertex_descriptor&);
  void set_goal(const vertex_descriptor&);

  vertex_descriptor source() const;
  vertex_descriptor goal() const;

  bool solve();
  bool solved() const;
  bool solution_contains(vertex_descriptor u) const;

private:
    // The grid underlying the maze
  grid m_grid;

  // The underlying maze grid with barrier vertices filtered out
  filtered_grid m_barrier_grid;

  // The barriers in the maze
  vertex_set m_barriers;

  AStar_solver solver;
};

struct astar_goal_visitor : public boost::default_astar_visitor {
  astar_goal_visitor(vertex_descriptor goal):m_goal(goal) {};

  void examine_vertex(vertex_descriptor u, const filtered_grid&) {
    if (u == m_goal)
      throw found_goal();
  }

private:
  vertex_descriptor m_goal;
};


