/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "astar_solver.h"

AStar_solver::AStar_solver()   
{
}

bool AStar_solver::solve(filtered_grid maze, vertex_descriptor s, vertex_descriptor g)
{
  boost::static_property_map<std::size_t> weight(1); // (1)
  typedef boost::unordered_map<vertex_descriptor, vertex_descriptor, vertex_hash> pred_map;
  typedef boost::unordered_map<vertex_descriptor, std::size_t, vertex_hash> dist_map;
  pred_map predecessor;
  dist_map distance;
  boost::associative_property_map<pred_map> pred_pmap(predecessor);
  boost::associative_property_map<dist_map> dist_pmap(distance);
  
  //Set the goal in heuristic and visitor.
  heuristic.set_goal(g);
  visitor.set_goal(g);

  try {
    astar_search(maze, s, heuristic,
                 boost::weight_map(weight).predecessor_map(pred_pmap).distance_map(dist_pmap).visitor(visitor) );
  } 
  catch(found_goal fg) {
    // Walk backwards from the goal through the predecessor chain adding
    // vertices to the solution path.
    std::cout << "asearch done...\n";
    for (vertex_descriptor u = g; u != s; u = predecessor[u])
      solution_.insert(u);

    solution_.insert(s);
    solution_length = distance[g];
    return true;
  }
 return false;
}


bool AStar_solver::solved() const {
  return !solution_.empty();
}



bool AStar_solver::solution_contains(vertex_descriptor u) const
{
  return solution_.find(u) != solution_.end();
}

const vertex_set& AStar_solver::solution() const
{
  return solution_;
}

const std::size_t AStar_solver::length() const
{
  return solution_length;
}
