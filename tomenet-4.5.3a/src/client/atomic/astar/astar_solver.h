/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "diagonal_heuristic.h"


struct found_goal {};

struct astar_goal_visitor : public boost::default_astar_visitor {
 astar_goal_visitor() {};
 void set_goal(vertex_descriptor g) {m_goal = g;}
 void examine_vertex(vertex_descriptor u, const filtered_grid&) {
   if (u == m_goal)
     throw found_goal();
 }
 
 private:
 vertex_descriptor m_goal;
};

class AStar_solver {
 public:
  AStar_solver();
  bool solve(filtered_grid m, vertex_descriptor s, vertex_descriptor g);
  bool solved() const;
  bool solution_contains(vertex_descriptor) const;
  const vertex_set& solution() const;
  const std::size_t length() const;

 private:
  diagonal_heuristic heuristic;
  astar_goal_visitor visitor;
  vertex_set solution_;
  std::size_t solution_length;
};
