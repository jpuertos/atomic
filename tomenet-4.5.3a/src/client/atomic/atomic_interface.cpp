/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "AtomicInterface.h"
#include <unistd.h>

AtomicInterface* atomic_interface;


int last_tick = ticks;

extern "C" void atomic_init(void) {
  atomic_interface = new AtomicInterface();
}

extern "C" void atomic_request_command(void)
{
  /*
  if (ticks > last_tick + 10)
  {
    atomic_interface.r();
    last_tick = ticks;
  }
  */
}

extern "C" void atomic_update(void)
{
  atomic_interface->update();
  /*
  if (ticks >= last_tick + 1)
    { 
      atomic_interface->update();
      last_tick = ticks;
    }
  */
}

extern "C" void atomic_passmsg(char* msg)
{
  atomic_interface->get_message(msg);
}

extern "C" void atomic_grab_tixel(char x, char y, unsigned char a, char c) {
  atomic_interface->grab_tixel(x, y, a, c);
}

extern "C" void atomic_grab_message(char* s) {
  atomic_interface->grab_message(s);
}


extern "C" void atomic_grab_updated_attr(int attr)
{
  atomic_interface->grab_updated_attr(attr);
}

extern "C" void atomic_grab_gold( int g,  int b)
{
  atomic_interface->grab_gold(g, b);
}

extern "C" void atomic_grab_state( bool b1,  bool b2,  bool b3)
{
  atomic_interface->grab_state(b1, b2, b3);
}

extern "C" void atomic_grab_confused( bool b)
{
  atomic_interface->grab_confused(b);
}

extern "C" void atomic_grab_poison( bool b)
{
  atomic_interface->grab_confused(b);
}

extern "C" void atomic_grab_food( int f)
{
  atomic_interface->grab_food(f);
}

extern "C" void atomic_grab_fear( bool b)
{
  atomic_interface->grab_fear(b);
}

extern "C" void atomic_grab_speed( int s)
{
  atomic_interface->grab_speed(s);
}

extern "C" void atomic_grab_cut( bool b)
{
  atomic_interface->grab_cut(b);
}

extern "C" void atomic_grab_blind( bool b)
{
  atomic_interface->grab_blind(b);
}

extern "C" void atomic_grab_stun( bool b)
{
  atomic_interface->grab_stun(b);
}
