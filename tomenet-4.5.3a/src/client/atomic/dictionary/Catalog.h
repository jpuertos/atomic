/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_CATALOG__
#define __ATOMIC_CATALOG__

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <list>
#include <string>

#include "Tile.h"
#include "RaceInfo.h"
#include "TerrainInfo.h"
#include "KindInfo.h"

namespace atomic {

  struct by_name{};
  struct by_tile{};

  /*
  struct char_extractor {
    typedef char_type result_type;
    const result_type& operator()(const Tile& t) const { return t.first; }    
  };

  struct attr_extractor {
    typedef attr_type result_type;
    const result_type& operator()(const Tile& t) const { return t.second; }
  };

  */
  template <typename T>
  using CatalogContainer = boost::multi_index::multi_index_container<
    T,
    boost::multi_index::indexed_by <
      boost::multi_index::hashed_non_unique< boost::multi_index::tag<by_tile>,
                                             boost::multi_index::member<T, Tile, &T::tile>,
                                             boost::hash<Tile>,
                                             std::equal_to<Tile> >,

      boost::multi_index::hashed_non_unique< boost::multi_index::tag<by_name>,
                                             boost::multi_index::member<T, std::string, &T::name>,
                                             boost::hash<std::string>,
                                             std::equal_to<std::string> >
    >
  >;

  template <typename T>
  class Catalog : public CatalogContainer<T> {
    public:
      Catalog();
  };

#include "CatalogRace.cpp"
#include "CatalogTerrain.cpp"
#include "CatalogKind.cpp"

}

#endif
