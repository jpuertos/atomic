/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_EGORACECATALOG__
#define __ATOMIC_EGORACECATALOG__

#include <set>
#include <vector>
#include <boost/unordered/unordered_map.hpp>
//#include <boost/bimap/bimap.hpp>
//#include <cstring>
#include "EgoRaceInfo.h"

namespace atomic {

    struct by_char {};
    struct by_rflag {};

    class CatalogEgoRace {

    public:
      typedef boost::unordered_map<std::string, re_info> ego_container_t;

      /*
      typedef boost::bimap<boost::bimap::tagged<char, by_char>,
                           boost::bimap::tagged <RaceFlag, by_rflag> > char_rflag_bm_t;
      */
      typedef std::vector<ego_container_t::const_iterator> ego_vector_t;
      typedef boost::unordered_map<Tile, ego_vector_t> cartesian_lookup_t;

      CatalogEgoRace();
      ego_vector_t find(const Tile&); // Possible hack is to search for ('G', '*') to find the Ghost ego race... if char = [s, z, L, G]
      ego_vector_t find(const std::string&);      

    private:
      void populate_container(void);

    private:
      ego_container_t _container;
      cartesian_lookup_t _cartesian;
      
    };
}

#endif
