/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "CatalogEgoRace.h"

using namespace atomic;

CatalogEgoRace::CatalogEgoRace()
{
  populate_container();

}

#warning The following logic may not be very accurate.
CatalogEgoRace::ego_vector_t CatalogEgoRace::find(const Tile& t)
{
  if (t.first == 's' || t.first == 'z' || t.first == 'L' ||  t.first == 'G') {
    auto itr_bychar = _cartesian.find(Tile(t.first, '*'));
    if (itr_bychar != _cartesian.end()) {
      return itr_bychar->second;
    }
  }

  auto itr_bycolor = _cartesian.find(t);  
  if (itr_bycolor != _cartesian.end()) {
    return itr_bycolor->second;
  }

  return CatalogEgoRace::ego_vector_t();
}

void CatalogEgoRace::populate_container(void)
{
  // CODEGEN ENTRY: Insert

}
