#ifndef __ATOMIC_CATALOG_DUNGEON__
#define __ATOMIC_CATALOG_DUNGEON__

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <vector>
#include <string>

#include "DungeonInfo.h"

namespace atomic {

  typedef boost::multi_index::multi_index_container<
    d_info,
    boost::multi_index::indexed_by <
      boost::multi_index::hashed_unique< boost::multi_index::member<d_info, int, &d_info::tome_ind> >,
      boost::multi_index::hashed_unique< boost::multi_index::member<d_info, std::string, &d_info::name> >,
      boost::multi_index::hashed_unique< boost::multi_index::member<d_info, std::string, &d_info::short_name> >
    >
  > DungeonContainer;

  typedef boost::multi_index::multi_index_container<
    sd_info,
    boost::multi_index::indexed_by <
      boost::multi_index::hashed_unique< boost::multi_index::member<sd_info, Coord2D, &sd_info::loc> >,
      boost::multi_index::hashed_unique< boost::multi_index::member<sd_info, std::string, &sd_info::name> >
    >
  > ServerDungeonContainer;

  typedef std::pair<
    ServerDungeonContainer::nth_index<0>::type::const_iterator,
    DungeonContainer::nth_index<0>::type::const_iterator
  > Dungeon;

  class DungeonCatalog {
    public:
      DungeonCatalog();
      Dungeon find(const Coord2D&);
      Dungeon find(const std::string&);

    private:
      DungeonContainer* dungeon;
      ServerDungeonContainer* server_dungeon;
  };
}

#endif
