#include "DungeonCatalog.h"

using namespace atomic;

DungeonCatalog::DungeonCatalog()
{
  dungeon = new DungeonContainer(boost::make_tuple( boost::make_tuple(1, boost::multi_index::member<d_info, int, &d_info::tome_ind>(),
								      boost::hash<int>(), std::equal_to<int>()),
						    boost::make_tuple(1, boost::multi_index::member<d_info, std::string, &d_info::name>(),
								      boost::hash<std::string>(), std::equal_to<std::string>()),
						    boost::make_tuple(1, boost::multi_index::member<d_info, std::string, &d_info::short_name>(),
								      boost::hash<std::string>(), std::equal_to<std::string>())));

  //Game dungeons.
  DungeonContainer::nth_index<0>::type& dungeon_type = dungeon->get<0>();
  // CODEGEN ENTRY: Insert dung


  // Server dungeons list.
  server_dungeon = new ServerDungeonContainer(boost::make_tuple( boost::make_tuple(1, boost::multi_index::member<sd_info, Coord2D, &sd_info::loc>(),
										   boost::hash<Coord2D>(), std::equal_to<Coord2D>()),
								 boost::make_tuple(1, boost::multi_index::member<sd_info, std::string, &sd_info::name>(),
										   boost::hash<std::string>(), std::equal_to<std::string>())));

  ServerDungeonContainer::nth_index<0>::type& sdung_id = server_dungeon->get<0>();
  sdung_id.insert({{36,1}, "Dol Guldur", 57, 71, 28});
  sdung_id.insert({{8,4}, "The Heart of the Earth", 25, 37, 10});
  sdung_id.insert({{26,5}, "Angband", 67, 127, 30});
  sdung_id.insert({{48,8}, "Cirith Ungol", 25, 51, 10});
  sdung_id.insert({{1,10}, "The Small Water Cave", 32, 35, 15});
  sdung_id.insert({{3,12}, "The Illusory Castle", 35, 52, 10});
  sdung_id.insert({{1,13}, "Death Fate", 1, 1, 1});
  sdung_id.insert({{27,13}, "Mordor", 34, 66, 15});
  sdung_id.insert({{53,17}, "The Old Forest", 13, 26, 5});
  sdung_id.insert({{8,21}, "The Maze", 25, 38, 15});
  sdung_id.insert({{19,25}, "The Halls of Mandos", 1, 98, 1});
  sdung_id.insert({{31,25}, "The Orc Cave", 10, 23, 3});
  sdung_id.insert({{30,27}, "Erebor", 50, 61, 35});
  sdung_id.insert({{42,28}, "The Mines of Moria", 30, 50, 15});
  sdung_id.insert({{21,31}, "Mount Doom", 85, 100, 38});
  sdung_id.insert({{32,32}, "The Training Tower", 1, 2, 1});
  sdung_id.insert({{45,32}, "Mirkwood", 11, 34, 5});
  sdung_id.insert({{27,33}, "The Land of Rhun", 26, 41, 13});
  sdung_id.insert({{30,34}, "Ironman Deep Dive Challenge", 1, 127, 0});
  sdung_id.insert({{51,38}, "The Helcaraxe", 20, 41, 10});
  sdung_id.insert({{4,39}, "The Sandworm lair", 22, 30, 12});
  sdung_id.insert({{9,48}, "Nether Realm", 166, 196, 40});
  sdung_id.insert({{59,51}, "Angband", 67, 127, 30});
  sdung_id.insert({{63,54}, "The Sacred Land of Mountains", 40, 45, 20});
  sdung_id.insert({{19,55}, "Submerged Ruins", 35, 51, 15});
  sdung_id.insert({{25,58}, "The Paths of the Dead", 40, 70, 20});

}

Dungeon DungeonCatalog::find(const Coord2D& loc) {
  ServerDungeonContainer::nth_index<0>::type::const_iterator sdung_it = server_dungeon->get<0>().find(loc);
  DungeonContainer::nth_index<1>::type::const_iterator dung_it = dungeon->get<1>().find(sdung_it->name);
  return Dungeon(sdung_it, dungeon->project<0>(dung_it));
}

Dungeon DungeonCatalog::find(const std::string& name) {
  ServerDungeonContainer::nth_index<1>::type::const_iterator sdung_it = server_dungeon->get<1>().find(name);
  DungeonContainer::nth_index<1>::type::const_iterator dung_it = dungeon->get<1>().find(name);
  return Dungeon(server_dungeon->project<0>(sdung_it), dungeon->project<0>(dung_it));
}

