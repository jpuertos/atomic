/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_DUNGEON_INFO__
#define __ATOMIC_DUNGEON_INFO__

#include <vector>
#include <bitset>
#include <string>

#include "../Coords.h"

namespace atomic {

// CODEGEN ENTRY: Enums

struct Dungeon_xinfo {
  int min_depth;
  int max_depth;
  int min_player_level;
  int next_dungeon;
  int min_alloc; //Alloc chances ?
  int max_alloc;
};

struct Dungeon_loot {
  int treasure;
  int combat;
  int magic;
  int tool;
};


typedef std::pair<std::size_t, std::size_t> FeatureProb; 
typedef std::vector< FeatureProb > FeatureList; 

struct d_info {
  int tome_ind;
  std::string name;
  std::string short_name;
  std::string long_name;
  Dungeon_xinfo xinfo;
  FeatureList floor;
  std::vector<std::size_t> floor_end;
  FeatureList wall;
  std::vector<std::size_t> wall_end;
  Dungeon_loot loot;
  std::bitset<std::size_t(DungeonFlag::_size_)> flags;
};

// Dungeon entry at server conf.
struct sd_info {
  Coord2D loc; // Location in world coords.
  std::string name;
  std::size_t start_level;
  std::size_t end_level;
  std::size_t req_level;
};
}
#endif
