/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef __ATOMIC_EGORACE_INFO__
#define __ATOMIC_EGORACE_INFO__

#include "RaceInfo.h"

namespace atomic {
enum class EgoModType {
  none,
  add,
  sub,
  percent,
  set
};

typedef std::pair<EgoModType, int> EgoModifier;

typedef std::pair<RaceBlow, EgoModifier> RaceBlowMod;

struct EgoRace_info {
  EgoModifier speed;
  EgoModifier health_dices;
  EgoModifier health_sides;
  EgoModifier vision;
  EgoModifier ac;
  EgoModifier alertness;
};


struct EgoRace_xinfo {
  EgoModifier depth;
  int rarity;
  EgoModifier corpse_weight;
  EgoModifier xp4kill;
  char        place;  // 'A', 'B', what the hell is that?
};


struct re_info {
  int tome_ind;
  Tile tile;
  std::string name;
  EgoRace_info info;
  EgoRace_xinfo xinfo;
  //std::vector<RaceBlowMod> blows;
  Race_spell added_spell;
  std::vector<SpellType> removed_spell;
  std::vector<RaceFlag> flags_must;
  std::vector<RaceFlag> flags_mustnot;
  std::vector<RaceFlag> flags_added;
  std::vector<RaceFlag> flags_removed;

};
}

#endif
