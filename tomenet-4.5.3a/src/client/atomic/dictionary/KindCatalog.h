#ifndef __ATOMIC_CATALOG_KIND__
#define __ATOMIC_CATALOG_KIND__

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <list>
#include <string>

#include "KindInfo.h"


namespace atomic {

 typedef boost::multi_index::multi_index_container<
    k_info,
    boost::multi_index::indexed_by <
      boost::multi_index::hashed_non_unique< boost::multi_index::member<k_info, std::string, &k_info::name> >,
      boost::multi_index::hashed_non_unique< boost::multi_index::member<k_info, Tile, &k_info::tile> >
    >
  > KindContainer;

 class KindCatalog {
    public:
      KindCatalog();
      std::list< typename KindContainer::nth_index<0>::type::const_iterator > find(const std::string&);
      std::list< typename KindContainer::nth_index<1>::type::const_iterator > find(const Tile&);

    private:
      KindContainer* kind;
 };

}

#endif
