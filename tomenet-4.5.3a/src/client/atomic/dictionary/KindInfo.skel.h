/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_KIND_INFO__
#define __ATOMIC_KIND_INFO__

#include <vector>
#include <bitset>
#include <string>

#include "Tile.h"

namespace atomic {

  /*
   * Dont removed following comment! is used as an entry point for codegen.
   *
   * KindFlag
   */

// CODEGEN ENTRY: Enums

struct Kind_info {
  int tval;
  int sval;
  int pval;
};

struct Kind_xinfo {
  int depth;
  int rarity;
  int weight;
  int price;
};

struct Kind_power {
  int ac;
  int dices; //
  int sides; // Base dam.
  int tohit;
  int todam;
  int ac_plus;
};

struct k_info {
  int tome_ind;
  Tile tile;
  std::string name;
  Kind_info info;
  Kind_xinfo xinfo;
  //std::vector<std::pair<int,int>> a; //WTF is this?
  Kind_power power;
  std::bitset<std::size_t(KindFlag::_size_)> flags;
};

}

#endif
