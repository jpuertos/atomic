/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_RACE_INFO__
#define __ATOMIC_RACE_INFO__

#include <bitset>
#include <vector>
#include <string>

#include "Tile.h"

namespace atomic {

  /*
   * Dont removed following comment! is used as an entry point for codegen.
   *
   * RaceFlag
   * AttackMethod
   * AttackEffect
   * SpellFreq
   * SpellType
   */

// CODEGEN ENTRY: Enums


struct Race_info {
  int speed;
  int health_dices;
  int health_sides;
  int vision;
  int ac;
  int alertness;
};

struct Race_xinfo {
  int depth;
  int rarity;
  int corpse_weight;
  int xp4kill;
};

struct Race_slots {
  int weapons;
  int torso;
  int arms;
  int finger;
  int head;
  int leg;
};

struct Race_drop {
  int treasure;
  int combat;
  int magic;
  int tool;
};

struct RaceBlow {
    AttackMethod method;
    AttackEffect effect;
    int dices;
    int sides;
};

struct Race_spell {
  SpellFreq freq;
  std::vector<SpellType> type;
};

struct r_info {
  int tome_ind;
  Tile tile;
  std::string name;
  //std::string desc; //We will rarely use this.
  Race_info info;
  Race_xinfo xinfo;
  Race_slots slots;
  Race_drop drop;
  std::vector<RaceBlow> blows;
  Race_spell spell;
  std::bitset<std::size_t(RaceFlag::_size_)> flags;
};

}

#endif
