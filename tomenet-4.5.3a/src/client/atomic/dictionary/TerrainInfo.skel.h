/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_TERRAIN_INFO__
#define __ATOMIC_TERRAIN_INFO__

#include <string>
#include <bitset>
#include "Tile.h"

namespace atomic {

 /*
   * Dont removed following comment! is used as an entry point fpor codegen.
   */

// CODEGEN ENTRY: Enums

  struct TerrainEffect {
    TerrainEffectType type;
    int rare;
    int dices;
    int dam;
  };

  struct tf_info {
    int tome_ind;
    Tile tile;
    int mimic;
    std::string name;
    std::bitset<std::size_t(TerrainFlag::_size_)> flags;
    TerrainEffect e;
  };
}

#endif
