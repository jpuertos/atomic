/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_TILE__
#define __ATOMIC_TILE__

#include <iostream>

// To Prevent boost::hash specializations get redefined.
#undef bool

namespace atomic {

  typedef char          char_type;
  typedef unsigned char attr_type;

  /*
   *  A 'byte' is defined as an unsigned char. We avoid including its definition
   *  to provent collisions with boost.
   *
   *  We make use of std::pair, to avoid having to define "hash_value" and
   *  std::equal_to
   */

  typedef std::pair<char_type, attr_type> Tile;

}

std::ostream& operator<<(std::ostream&, const atomic::Tile&);

#endif
