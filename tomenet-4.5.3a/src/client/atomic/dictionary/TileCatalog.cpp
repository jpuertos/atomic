#include <algorithm>
#include "TileCatalog.h"
#include "../import_from_tomenet.h"

using namespace atomic;

TileCatalog::TileCatalog()
{
}


TileTuple TileCatalog::find(const Tile& tile) {
  Tile t = Tile(tile.first, atomic_color_attr_to_char(tile.second));

  //ego.find(t);
  return TileTuple( race.get<by_tile>().equal_range(t),
		    terrain.get<by_tile>().equal_range(t),
		    kind.get<by_tile>().equal_range(t),
		    //CatalogEgoRace::ego_vector_t());
                    ego.find(t));
}

std::ostream& operator<<(std::ostream& os, const atomic::TileTuple& tup)
{
  
  bool has_race = (tup.get<0>().first != tup.get<0>().second);
  //  bool has_egorace_char = (tup.get<1>().first != tup.get<1>().second);
  // bool has_egorace_color = (tup.get<2>().first != tup.get<2>().second);
  bool has_terrain = (tup.get<1>().first != tup.get<1>().second);
  bool has_kind = (tup.get<2>().first != tup.get<2>().second);
  
  if (has_race) {
    os << "R: ";
    Catalog<r_info>::const_iterator it = tup.get<0>().first;
    for (it; it != tup.get<0>().second; it++)
      os << it->name << " | ";
    os << std::endl;
  }

  if (has_terrain) {
    if (has_race) os << "           ";
    os << "T: ";
    Catalog<tf_info>::const_iterator it = tup.get<1>().first;
    for (it; it != tup.get<1>().second; it++)
      os << it->name << " | ";
    os << std::endl;
  }

  if (has_kind) {
    if (has_race || has_terrain) os << "           ";
    os << "K: ";
    Catalog<k_info>::const_iterator it = tup.get<2>().first;
    for (it; it != tup.get<2>().second; it++)
      os << it->name << " | ";
    os << std::endl;
  }

  if (!has_race && !has_terrain && !has_kind)
    os << "*NO MATCH*" << std::endl;

  for (auto itr = tup.get<3>().begin(); itr != tup.get<3>().end(); ++itr)
    os << "E: " << (*itr)->first << " | ";
  os << std::endl;
  
  return os;
}
