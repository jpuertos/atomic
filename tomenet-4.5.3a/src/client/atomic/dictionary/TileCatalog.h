/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef __ATOMIC_TILE_CATALOG__
#define __ATOMIC_TILE_CATALOG__

#include "Tile.h"
#include "Catalog.h"
#include "CatalogEgoRace.h"

namespace atomic {
  
  template <typename T>
  using CatalogResult = std::pair< typename Catalog<T>::const_iterator,
                                   typename Catalog<T>::const_iterator >;
  
  typedef boost::tuple<
    CatalogResult<r_info>,
    CatalogResult<tf_info>,
    CatalogResult<k_info>,
    CatalogEgoRace::ego_vector_t
  > TileTuple;

  class TileCatalog {
    public:
      TileCatalog();
      TileTuple find(const Tile&);
      TileTuple find(const std::string&);

    private:
      Catalog<r_info> race;
      Catalog<tf_info> terrain;
      Catalog<k_info> kind;
      CatalogEgoRace ego;
  };
}

std::ostream& operator<<(std::ostream& os, const atomic::TileTuple&);

#endif
