/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*
#  byte type;            /* this goes into sval */
#  s16b probability;     /* probability of existence in 1000 */
#  s16b another;         /* does this trap easily combine in 1000 */
#  s16b pvalinc;         /* how much does this trap attribute to pval */
#  byte difficulty;      /* how difficult to disarm */
#  byte level;           /* minimum level - disenchantment trap at 200' is */
#                        /* not so nice */
#  byte color;
#  cptr name;            /* what name does this trap have */
#
#  d TERM_DARK  |r TERM_RED  |D TERM_L_DARK |R TERM_L_RED
#  w TERM_WHITE |g TERM_GREEN|W TERM_L_WHITE|G TERM_L_GREEN
#  s TERM_SLATE |b TERM_BLUE |v TERM_VIOLET |B TERM_L_BLUE
#  o TERM_ORANGE|u TERM_UMBER|y TERM_YELLOW |U TERM_L_UMBER
#
# b blue   for stat traps
# w white  for teleport traps
# o orange for dungeon rearrangment traps
# v violet for summoning traps
# y yellow for stealing/equipment traps
# r red    for other character affecting traps
# g green  for elemental bolt trap
# B umber  for elemental ball trap
# R l red  for arrow/dagger traps
# W        for compound trap!!!
# G        for devine trap??
# don't use U or you'll get trapped doors that are indistinguishable from untrapped doors!
#
#  an unknown character is multi-hued!
#
# N:type:name
# I:diff:prob:another:pval:minlevel:damage:color
# I:diff:prob:            :minlevel:      :color
# D:description

 */

#ifndef __ATOMIC_TRAP_INFO__
#define __ATOMIC_TRAP_INFO__

#include <bitset>
#include <vector>
#include <string>

#include "Tile.h"

namespace atomic {

  struct Trap_info {
    int probability;
    int another;
    int pvalinc;         /* how much does this trap attribute to pval */
    int difficulty;      /* how difficult to disarm */
    int level;           /* minimum level - disenchantment trap at 200' is */
    int dices;
    int sides;
    
  };

  struct tr_info {
    int tome_ind;
    Tile tile;
    std::string name;            /* what name does this trap have */
    std::string desc;
    Trap_info info;
  };
}
