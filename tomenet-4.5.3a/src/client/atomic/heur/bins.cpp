

#include <math.h>
#include <vector>
#include <iostream>

std::vector<double> calc_bin_intervals(double base, int lower, int upper)
{
  std::vector<double> bin_intervals;
 
  std::cout <<  "0.0, ";
  for(int i = lower; i <= upper; i++) {
    bin_intervals.push_back(pow(base, i));
    std::cout << bin_intervals[i-lower] << ", ";
  }
  std::cout << std::endl;
  return bin_intervals;
}

int get_bucket(std::vector<double> bin_intervals, double reference, double x)
{
  double v;
  v = x / reference;


  int i = 0;
  while( i < bin_intervals.size() && v > bin_intervals[i])
    {   
      i++;
    }

  return i;
}


int main(int argc, char** argv)
{
  std::cout << get_bucket(calc_bin_intervals(2, -4, 1), 1.0, 0.26) << std::endl;
}
