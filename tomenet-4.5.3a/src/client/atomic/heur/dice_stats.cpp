
/*
 *
 * http://stats.stackexchange.com/questions/3614/how-to-easily-determine-the-results-distribution-for-multiple-dice 
 *
 */
#include <iostream>

double expected_value(double d, double s)
{
  return d*(s+1)/2;
}

double variance(double d, double s)
{
  return d*(s*s-1)/12;
}

