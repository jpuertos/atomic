#include <boost/math/distributions/normal.hpp>
#include <iostream>
#include <math.h>
#include "dice_stats.h"

/*
 * Modeled at
 * melee1.c:73
 *
 * power is given by the atack type!
 * totla_ac MUST be equal to
 *
 *
 * IMPORTANT, monsters can make upto 4 blows per round.
 *
 * So, when describeing a Monster attacking features, we will have
 *
 *  FV monster blow = (blow_prob_hit, blow_relative_mean, blow_relative_standard_dev)
 *
 */

/*
int power(AttackEffect effect)
{
  switch (effect)
    {
    case acid:  	power =  0; break;
    case blind: 	power =  2; break;
    case cold:  	power = 10; break;
    case confuse:       power = 10; break;
    case disarm:	power = 60; break;
    case disease:   	power =  5; break;
    case eat_food:      power =  5; break;
    case eat_gold:      power =  5; break;
    case eat_item:      power =  5; break;
    case eat_lite:      power =  5; break;
    case elec:  	power = 10; break;
    case exp_10:        power =  5; break;
    case exp_20:        power =  5; break;
    case exp_40:        power =  5; break;
    case exp_80:        power =  5; break;
    case famine:	power = 20; break;
    case fire:  	power = 10; break;
    case hallu:     	power = 10; break;
    case hurt:  	power = 60; break;
    case lose_all:      power =  2; break;
    case lose_chr:      power =  0; break;
    case lose_con:      power =  0; break;
    case lose_dex:      power =  0; break;
    case lose_int:      power =  0; break;
    case lose_str:      power =  0; break;
    case lose_wis:      power =  0; break;
    case paralyze:      power =  2; break;
    case parasite:  	power =  5; break;
    case poison:        power =  5; break;
    case sanity:    	power = 60; break;
    case seduce:	power = 80; break;
    case shatter:       power = 60; break;
    case terrify:       power = 10; break;
    case time:      	power =  5; break;
    case un_bonus:      power = 20; break;
    case un_power:      power = 15; break;
    }

  return power;
}

bool bypass_ac(AttackMethod method)
{
  switch(method)
    {
    case wail:
    case spore:
    case beg:
    case insult:
    case moan:
    case show:
    case whisper:
    case explode:
      return true;
    default:
      return false;
    }
}

bool bypass_shield(AttackMethod method)
{
 switch(method)
    {
    case wail:
    case spore:
    case beg:
    case insult:
    case moan:
    case show:
    case whisper:
      return true;
    default:
      return false;
    }
}

bool bypass_weapon(AttackMethod method)
{
 switch(method)
    {
    case wail:
    case spore:
    case beg:
    case insult:
    case moan:
    case show:
    case whisper:
    case explode:
    case drool:
      return true;
    default:
      return false;
    }
}

*/

#warning TODO: If the monster is studed, its level drops by 20% or 30%
double prob_hit(int ac, int to_a, int power, int mon_level, bool bypass_ac)
{
  double prob;
  int i = (power + (mon_level * 3)); //The attack quality (its constant).

  // Total AC.
  int total_ac = to_a;
  if (!bypass_ac)
    total_ac += ac;

  double mean = expected_value(1.0, i);
  double sd = sqrt(variance(1.0, i));
  boost::math::normal quality_attack(mean, sd);

  // %5 always hit and 5% never hit.
  //if ((i > 0) && (randint(i) > ((total_ac * 3) / 4))) return (TRUE);

  prob = 0.05 + (1 - boost::math::cdf(quality_attack, (total_ac * 3) / 4));
  if (prob > 0.95)
    return 0.95;
  
  return prob;
}

int main()
{
  int ac = 0;
  int l = 5;
  int p = 60;
  std::cout << "AC:" << ac << ", Monster Level:" << l << ", Attack Power:" << p;
  std::cout << " !Hit prob:" << prob_hit(ac, 0, p, l, false) << std::endl;

  ac = 15; l = 5; p = 60;
  std::cout << "AC:" << ac << ", Monster Level:" << l << ", Attack Power:" << p;
  std::cout << " !Hit prob:" << prob_hit(ac, 0, p, l, false) << std::endl;

  ac = 30; l = 5; p = 60;
  std::cout << "AC:" << ac << ", Monster Level:" << l << ", Attack Power:" << p;
  std::cout << " !Hit prob:" << prob_hit(ac, 0, p, l, false) << std::endl;

  ac = 60; l = 5; p = 60;
  std::cout << "AC:" << ac << ", Monster Level:" << l << ", Attack Power:" << p;
  std::cout << " !Hit prob:" << prob_hit(ac, 0, p, l, false) << std::endl;

  ac = 100; l = 20; p = 60;
  std::cout << "AC:" << ac << ", Monster Level:" << l << ", Attack Power:" << p;
  std::cout << " !Hit prob:" << prob_hit(ac, 0, p, l, false) << std::endl;

}
