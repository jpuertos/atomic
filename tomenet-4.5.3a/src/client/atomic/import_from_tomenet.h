/***************************************************************************
 *   Copyright (C) 2012 by Juan V. Puertos                                 *
 *   juanvi.puertos@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef __EXPORT_TO_ATOMIC__
#define __EXPORT_TO_ATOMIC__

#include "../../common/z-util.h"
#include "../../common/z-virt.h"
#include "../../common/z-form.h"
#include "../../common/z-rand.h"
#include "../z-term.h"
#include "../../config.h"
#include "../../common/defines.h"
#include "../../common/types.h"

extern "C" {

  /* For MapLayer, from z-term */

  char atomic_color_attr_to_char(int a);
  int atomic_color_char_to_attr(char c);
  
 /* c-tables.c */
  byte *adj_mag_stat;
 byte *adj_mag_fail;
 s16b ddx[10];
 s16b ddy[10];
 char hexsym[16];
 option_type *option_info;
 cptr stat_names[6];
 cptr stat_names_reduced[6];
 char ang_term_name[ANGBAND_TERM_MAX][40];
 cptr window_flag_desc[8];
 monster_spell_type monster_spells4[32];
 monster_spell_type monster_spells5[32];
 monster_spell_type monster_spells6[32];
 cptr melee_techniques[16];
 cptr ranged_techniques[16];

/* variable.c */
 bool atomic_enabled; // Allows to enable/disable ATOMIC.
 bool atomic_paused;  // Allows to stop/resume ATOMIC,
 bool c_quit;
 char meta_address[MAX_CHARS];
 char nick[MAX_CHARS];
 char pass[MAX_CHARS];
 char svname[MAX_CHARS];
 char path[1024];
 char real_name[MAX_CHARS];
 char server_name[MAX_CHARS];
 s32b server_port;
 char cname[MAX_CHARS];
 char message_history[MSG_HISTORY_MAX][MSG_LEN];
 char message_history_chat[MSG_HISTORY_MAX][MSG_LEN];
 char message_history_msgnochat[MSG_HISTORY_MAX][MSG_LEN];
 byte hist_end;
 bool hist_looped;
 byte hist_chat_end;
 bool hist_chat_looped;
 object_type inventory[INVEN_TOTAL];
 char inventory_name[INVEN_TOTAL][ONAME_LEN];
 int inventory_inscription[INVEN_TOTAL];
 int inventory_inscription_len[INVEN_TOTAL];
 store_type store;
 c_store_extra c_store;
 int store_prices[STORE_INVEN_MAX];
 char store_names[STORE_INVEN_MAX][ONAME_LEN];
 s16b store_num;
 char spell_info[MAX_REALM + 9][9][9][80];
 char party_info_name[90];
 char party_info_members[20];
 char party_info_owner[50];
 setup_t Setup;
 client_setup_t Client_setup;
 bool shopping;
 s16b last_line_info;
 s32b cur_line;
 s32b max_line;
 player_type Players[2];
 player_type *p_ptr;
 c_player_extra c_player;
 c_player_extra *c_p_ptr;
 s32b exp_adv;
 s16b command_see;
 s16b command_gap;
 s16b command_wrk;
 bool item_tester_full;
 byte item_tester_tval;
 bool (*item_tester_hook)(object_type *o_ptr);
 int special_line_type;
 bool inkey_base;
 bool inkey_scan;
 bool inkey_max_line; /* Make inkey() exit if we receive a 'max_line' value */
 bool inkey_flag; /* We're currently reading keyboard input (via inkey()) */
 bool inkey_interact_macros; /* In macro menu, no macros may act */
 bool inkey_msg;/* A chat message is currently being entered */
 s16b macro__num;
 cptr *macro__pat;
 cptr *macro__act;
 bool *macro__cmd;
 bool *macro__hyb;
 char *macro__buf;
 char recorded_macro[160];
 bool recording_macro;
 s32b message__next;
 s32b message__last;
 s32b message__head;
 s32b message__tail;
 s32b *message__ptr;
 char *message__buf;
 s32b message__next_chat;
 s32b message__last_chat;
 s32b message__head_chat;
 s32b message__tail_chat;
 s32b *message__ptr_chat;
 char *message__buf_chat;
 s32b message__next_msgnochat;
 s32b message__last_msgnochat;
 s32b message__head_msgnochat;
 s32b message__tail_msgnochat;
 s32b *message__ptr_msgnochat;
 char *message__buf_msgnochat;
 bool msg_flag;
 term *ang_term[ANGBAND_TERM_MAX];
 u32b window_flag[ANGBAND_TERM_MAX];
 byte color_table[256][4];
 cptr ANGBAND_SYS;
 byte keymap_cmds[128];
 byte keymap_dirs[128];
 s16b command_cmd;
 s16b command_dir;
 s16b race;
  //s16b class;
 s16b sex;
 s16b mode;
 s16b trait;
/* DEG Stuff for new party client */
 s16b client;
 s16b class_extra;
 s16b stat_order[6];
 bool topline_icky;
 short screen_icky;
 bool party_mode;
 player_race *race_info;
 player_class *class_info;
 player_trait *trait_info;
 cptr ANGBAND_DIR;
 cptr ANGBAND_DIR_TEXT;
 cptr ANGBAND_DIR_USER;
 cptr ANGBAND_DIR_XTRA;
 cptr ANGBAND_DIR_SCPT;
 bool use_graphics;
 bool use_sound, use_sound_org;
 bool quiet_mode;
 bool noweather_mode;
 bool no_lua_updates;
 client_opts c_cfg;
 byte client_mode;
 u32b cfg_game_port;
 skill_type s_info[MAX_SKILLS];
 s16b flush_count;
 char reason[MAX_CHARS];	/* Receive_quit */

/*
 * The spell list of schools
 */
 s16b max_spells;
 spell_type *school_spells;
 s16b max_schools;
 school_type *schools;

/* Server ping statistics */
 int ping_id;
 int ping_times[60];
 bool lagometer_enabled;
 bool lagometer_open;

/* Chat mode: normal, party or level */
 char chat_mode;

/* Protocol to be used for connecting a server */
 s32b server_protocol;

/* Server version */
 version_type server_version;

/* Client fps used for polling keyboard input etc */
 int cfg_client_fps;

/* Client-side tracking of unique kills */
 byte r_unique[MAX_UNIQUES];
 char r_unique_name[MAX_UNIQUES][60];

/* Global variables for account options and password changing */
 bool acc_opt_screen;
 bool acc_got_info;
 s16b acc_flags;

/* Server detail flags */
 bool s_RPG;
 bool s_FUN;
 bool s_PARTY;
 bool s_ARCADE;
 bool s_TEST;
 bool s_RPG_ADMIN;

/* Server's temporary features flags */
 s32b sflags_TEMP;

/* Auto-inscriptions */
 char auto_inscription_match[MAX_AUTO_INSCRIPTIONS][40];
 char auto_inscription_tag[MAX_AUTO_INSCRIPTIONS][20];

/* Monster health memory (health_redraw) */
 int mon_health_num;
 byte mon_health_attr;

/* Location information memory (prt_depth) */
 bool depth_town;
 int depth_colour;
 int depth_colour_sector;
 char depth_name[MAX_CHARS];

/* Can macro triggers consist of multiple keys? */
 bool multi_key_macros;

/* Redraw skills if the menu is open */
 bool redraw_skills;
 bool redraw_store;

/* Special input requests (PKT_REQUEST_...) - C. Blue */
 bool request_pending;
 bool request_abort;


/* c-inven.c */
  /*
 s16b index_to_label(int i);
 bool item_tester_okay(object_type *o_ptr);
 cptr get_item_hook_find_obj_what;
 bool get_item_hook_find_obj(int *item, bool inven_first);
 bool (*get_item_extra_hook)(int *cp, bool inven_first);
 bool c_get_item(int *cp, cptr pmt, int mode);
  */
  
/* nclient.c (forer netclient.c) */
 int ticks, ticks10;
 void do_flicker(void);
 void do_mail(void);
 void update_ticks(void);
 void do_keepalive(void);
 void do_ping(void);
 int Net_setup(void);
 unsigned char Net_login(void);
 int Net_verify(char *real, char *nick, char *pass);
 int Net_init(int port);
 void Net_cleanup(void);
 int Net_flush(void);
 int Net_fd(void);
  // int Net_start(int sex, int race, int class);
 int Net_input(void);
 int Flush_queue(void);
 int next_frame(void);
 int Send_file_check(int ind, unsigned short id, char *fname);
 int Send_file_init(int ind, unsigned short id, char *fname);
 int Send_file_data(int ind, unsigned short id, char *buf, unsigned short len);
 int Send_file_end(int ind, unsigned short id);
 int Receive_file_data(int ind, unsigned short len, char *buffer);
 int Send_raw_key(int key);
 int Send_mimic(int spell);
 int Send_search(void);
 int Send_walk(int dir);
 int Send_run(int dir);
 int Send_drop(int item, int amt);
 int Send_drop_gold(s32b amt);
 int Send_tunnel(int dir);
 int Send_stay(void);
 int Send_toggle_search(void);
 int Send_rest(void);
 int Send_go_up(void);
 int Send_go_down(void);
 int Send_open(int dir);
 int Send_close(int dir);
 int Send_bash(int dir);
 int Send_disarm(int dir);
 int Send_wield(int item);
 int Send_observe(int item);
 int Send_take_off(int item);
 int Send_destroy(int item, int amt);
 int Send_inscribe(int item, cptr buf);
 int Send_uninscribe(int item);
 int Send_steal(int dir);
 int Send_quaff(int item);
 int Send_read(int item);
 int Send_aim(int item, int dir);
 int Send_use(int item);
 int Send_zap(int item);
 int Send_zap_dir(int item, int dir);
 int Send_fill(int item);
 int Send_eat(int item);
 int Send_activate(int item);
 int Send_activate_dir(int item, int dir);
 int Send_target(int dir);
 int Send_target_friendly(int dir);
 int Send_look(int dir);
 int Send_msg(cptr message);
 int Send_fire(int dir);
 int Send_throw(int item, int dir);
 int Send_item(int item);
 int Send_gain(int book, int spell);
 int Send_cast(int book, int spell);
 int Send_pray(int book, int spell);
 int Send_fight(int book, int spell);
 int Send_ghost(int ability);
 int Send_map(char mode);
 int Send_locate(int dir);
 int Send_store_purchase(int item, int amt);
 int Send_store_sell(int item, int amt);
 int Send_store_leave(void);
 int Send_store_confirm(void);
 int Send_redraw(char mode);
 int Send_special_line(int type, int line);
 int Send_party(s16b command, cptr buf);
 int Send_guild(s16b command, cptr buf);
 int Send_purchase_house(int dir);
 int Send_suicide(void);
 int Send_options(void);
 int Send_master(s16b command, cptr buf);
 int Send_clear_buffer(void);
 int Send_clear_actions(void);
 int Send_King(byte type);
 int Send_force_stack(int item);
 int Send_admin_house(int dir, cptr buf);
 int Send_spike(int dir);
 int Send_skill_mod(int i);
 int Send_store_examine(int item);
 int Send_store_command(int action, int item, int item2, int amt, int gold);
 int Send_activate_skill(int mkey, int book, int spell, int dir, int item, int aux);
 int Send_ping(void);
 int Send_sip(void);
 int Send_telekinesis(void);
 int Send_BBS(void);
 int Send_wield2(int item);
 int Send_cloak(void);
 int Send_inventory_revision(int revision);
 int Send_account_info(void);
 int Send_change_password(char *old_pass, char *new_pass);
 int Send_request_key(int id, char key);
 int Send_request_num(int id, int num);
 int Send_request_str(int id, char *str);
 int Send_request_cfr(int id, int cfr);
 int Send_request_save(int id);
}

#endif
