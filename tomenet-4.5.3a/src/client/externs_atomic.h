//#include "../common/h-basic.h"
#undef bool
#define bool bool_hack

#ifdef HAVE_STDBOOL_H
#include <stdbool.h>
#define TRUE true
#define FALSE false

#else

/* Use a char otherwise */
typedef char bool;

#undef TRUE
#undef FALSE

#define TRUE 1
#define FALSE 0 
#endif

extern bool atomic_enabled; // Allows to enable/disable ATOMIC.
extern bool atomic_paused;  // Allows to stop/resume ATOMIC,
extern char atomic_message[121]; //For passing comands to ATOMIC engine from the client.
